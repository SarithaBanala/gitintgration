com.conformiq.creator.structure.v15
creator.sequencetype qml_wsdl_import__seq__getTransactionsOnDate__0
"getTransactionsOnDate"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsOnDate__pfield__accountId__0 "accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsOnDate__pfield__onDate__0 "onDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.enum qml_wsdl_import__enum__transactionType__0 "transactionType"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.enumerationvalue qml_wsdl_import__transactionType__enumval__Credit__0
	"Credit";
	creator.enumerationvalue qml_wsdl_import__transactionType__enumval__Debit__0
	"Debit";
}
creator.sequencetype qml_wsdl_import__seq__transaction__0 "transaction"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__transaction__pfield__id__0 "id"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__transaction__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.enumerationfield qml_wsdl_import__transaction__enumfield__type__0
	"type"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__enum__transactionType__0
		optional;
	creator.primitivefield qml_wsdl_import__transaction__pfield__date__0 "date"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__transaction__pfield__amount__0
	"amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__transaction__pfield__description__0
	"description"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsOnDateResponse__0
"getTransactionsOnDateResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.sequencetype qml_wsdl_import__seq__ParaBankServiceException__0
"ParaBankServiceException"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__initializeDB__0 "initializeDB"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__initializeDBResponse__0
"initializeDBResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__getPositionHistory__0
"getPositionHistory"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getPositionHistory__pfield__positionId__0 "positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getPositionHistory__pfield__startDate__0 "startDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__getPositionHistory__pfield__endDate__0 "endDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__historyPoint__0 "historyPoint"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__historyPoint__pfield__symbol__0
	"symbol"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__historyPoint__pfield__date__0 "date"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__historyPoint__pfield__closingPrice__0
	"closingPrice"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__getPositionHistoryResponse__0
"getPositionHistoryResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse__sfield__historyPoint__0
	"historyPoint"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__historyPoint__0 array;
}
creator.sequencetype qml_wsdl_import__seq__transfer__0 "transfer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__transfer__pfield__fromAccountId__0
	"fromAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__transfer__pfield__toAccountId__0
	"toAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__transfer__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__transferResponse__0
"transferResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__transferResponse__pfield__transferReturn__0 "transferReturn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__getTransaction__0 "getTransaction"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransaction__pfield__transactionId__0 "transactionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionResponse__0
"getTransactionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse__sfield__transaction__0 "transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0;
}
creator.sequencetype qml_wsdl_import__seq__shutdownJmsListener__0
"shutdownJmsListener"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__shutdownJmsListenerResponse__0
"shutdownJmsListenerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__getCustomer__0 "getCustomer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getCustomer__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__address__0 "address"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__address__pfield__street__0 "street"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__address__pfield__city__0 "city"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__address__pfield__state__0 "state"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__address__pfield__zipCode__0 "zipCode"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__customer__0 "customer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__customer__pfield__id__0 "id"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__customer__pfield__firstName__0
	"firstName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__customer__pfield__lastName__0
	"lastName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.structuredfield qml_wsdl_import__customer__sfield__address__0
	"address"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__address__0
		optional;
	creator.primitivefield qml_wsdl_import__customer__pfield__phoneNumber__0
	"phoneNumber"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__customer__pfield__ssn__0 "ssn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__getCustomerResponse__0
"getCustomerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse__sfield__customer__0 "customer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__customer__0;
}
creator.sequencetype qml_wsdl_import__seq__updateCustomer__0 "updateCustomer"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__firstName__0
	"firstName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__lastName__0
	"lastName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__street__0
	"street"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__city__0
	"city"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__state__0
	"state"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__zipCode__0
	"zipCode"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__updateCustomer__pfield__phoneNumber__0 "phoneNumber"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__ssn__0 "ssn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__username__0
	"username"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__updateCustomer__pfield__password__0
	"password"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__updateCustomerResponse__0
"updateCustomerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__updateCustomerResponse__pfield__customerUpdateResult__0
	"customerUpdateResult"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__getPosition__0 "getPosition"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getPosition__pfield__positionId__0
	"positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.sequencetype qml_wsdl_import__seq__position__0 "position"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__position__pfield__positionId__0
	"positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__position__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__position__pfield__name__0 "name"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__position__pfield__symbol__0 "symbol"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__position__pfield__shares__0 "shares"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__position__pfield__purchasePrice__0
	"purchasePrice"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__getPositionResponse__0
"getPositionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0;
}
creator.sequencetype qml_wsdl_import__seq__sellPosition__0 "sellPosition"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__positionId__0
	"positionId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__sellPosition__pfield__shares__0
	"shares"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__sellPosition__pfield__pricePerShare__0 "pricePerShare"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__sellPositionResponse__0
"sellPositionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0 array;
}
creator.sequencetype qml_wsdl_import__seq__createAccount__0 "createAccount"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__createAccount__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__createAccount__pfield__newAccountType__0 "newAccountType"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__createAccount__pfield__fromAccountId__0 "fromAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.enum qml_wsdl_import__enum__accountType__0 "accountType"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.enumerationvalue qml_wsdl_import__accountType__enumval__CHECKING__0
	"CHECKING";
	creator.enumerationvalue qml_wsdl_import__accountType__enumval__SAVINGS__0
	"SAVINGS";
	creator.enumerationvalue qml_wsdl_import__accountType__enumval__LOAN__0
	"LOAN";
}
creator.sequencetype qml_wsdl_import__seq__account__0 "account"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__account__pfield__id__0 "id"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__account__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.enumerationfield qml_wsdl_import__account__enumfield__type__0 "type"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__enum__accountType__0
		optional;
	creator.primitivefield qml_wsdl_import__account__pfield__balance__0 "balance"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__createAccountResponse__0
"createAccountResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__createAccountResponse__sfield__account__0 "account"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__account__0;
}
creator.sequencetype qml_wsdl_import__seq__startupJmsListener__0
"startupJmsListener"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__startupJmsListenerResponse__0
"startupJmsListenerResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__login__0 "login"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__login__pfield__username__0 "username"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__login__pfield__password__0 "password"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__loginResponse__0 "loginResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield qml_wsdl_import__loginResponse__sfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__customer__0;
}
creator.sequencetype qml_wsdl_import__seq__deposit__0 "deposit"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__deposit__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__deposit__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__depositResponse__0
"depositResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__depositResponse__pfield__depositReturn__0 "depositReturn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByAmount__0
"getTransactionsByAmount"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsByAmount__pfield__accountId__0 "accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByAmount__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByAmountResponse__0
"getTransactionsByAmountResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.sequencetype qml_wsdl_import__seq__setParameter__0 "setParameter"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__setParameter__pfield__name__0 "name"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__setParameter__pfield__value__0
	"value"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__setParameterResponse__0
"setParameterResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__getPositions__0 "getPositions"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getPositions__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.sequencetype qml_wsdl_import__seq__getPositionsResponse__0
"getPositionsResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0 array;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByMonthAndType__0
"getTransactionsByMonthAndType"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsByMonthAndType__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByMonthAndType__pfield__month__0 "month"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByMonthAndType__pfield__type__0 "type"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype
qml_wsdl_import__seq__getTransactionsByMonthAndTypeResponse__0
"getTransactionsByMonthAndTypeResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.sequencetype qml_wsdl_import__seq__getTransactions__0
"getTransactions"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getTransactions__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsResponse__0
"getTransactionsResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.sequencetype qml_wsdl_import__seq__getTransactionsByToFromDate__0
"getTransactionsByToFromDate"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__getTransactionsByToFromDate__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByToFromDate__pfield__fromDate__0 "fromDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield
	qml_wsdl_import__getTransactionsByToFromDate__pfield__toDate__0 "toDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype
qml_wsdl_import__seq__getTransactionsByToFromDateResponse__0
"getTransactionsByToFromDateResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse__sfield__transaction__0
	"transaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transaction__0 array;
}
creator.sequencetype qml_wsdl_import__seq__requestLoan__0 "requestLoan"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__amount__0
	"amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__downPayment__0
	"downPayment"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__requestLoan__pfield__fromAccountId__0
	"fromAccountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__loanResponse__0 "loanResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__responseDate__0
	"responseDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield
	qml_wsdl_import__loanResponse__pfield__loanProviderName__0 "loanProviderName"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__approved__0
	"approved"
		namespace = "http://service.parabank.parasoft.com/"
		type = boolean;
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__message__0
	"message"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
	creator.primitivefield qml_wsdl_import__loanResponse__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String
		optional;
}
creator.sequencetype qml_wsdl_import__seq__requestLoanResponse__0
"requestLoanResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse__sfield__loanResponse__0 "loanResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__loanResponse__0;
}
creator.sequencetype qml_wsdl_import__seq__withdraw__0 "withdraw"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__withdraw__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__withdraw__pfield__amount__0 "amount"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__withdrawResponse__0
"withdrawResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield
	qml_wsdl_import__withdrawResponse__pfield__withdrawReturn__0 "withdrawReturn"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__getAccounts__0 "getAccounts"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getAccounts__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
}
creator.sequencetype qml_wsdl_import__seq__getAccountsResponse__0
"getAccountsResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse__sfield__account__0 "account"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__account__0 array;
}
creator.sequencetype qml_wsdl_import__seq__buyPosition__0 "buyPosition"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__customerId__0
	"customerId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__name__0 "name"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__symbol__0
	"symbol"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__shares__0
	"shares"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
	creator.primitivefield qml_wsdl_import__buyPosition__pfield__pricePerShare__0
	"pricePerShare"
		namespace = "http://service.parabank.parasoft.com/"
		type = String;
	creator.primitivefield qml0b44f207412947fbb0bdf6f542a00344 "new"
		type = String
		deleted;
}
creator.sequencetype qml_wsdl_import__seq__buyPositionResponse__0
"buyPositionResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse__sfield__position__0 "position"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__position__0 array;
}
creator.sequencetype qml_wsdl_import__seq__cleanDB__0 "cleanDB"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__cleanDBResponse__0
"cleanDBResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
}
creator.sequencetype qml_wsdl_import__seq__getAccount__0 "getAccount"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.primitivefield qml_wsdl_import__getAccount__pfield__accountId__0
	"accountId"
		namespace = "http://service.parabank.parasoft.com/"
		type = number;
}
creator.sequencetype qml_wsdl_import__seq__getAccountResponse__0
"getAccountResponse"
	namespace = "http://service.parabank.parasoft.com/"
{
	creator.structuredfield
	qml_wsdl_import__getAccountResponse__sfield__account__0 "account"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__account__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsOnDate__0
"SOAP Envelope for getTransactionsOnDate"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDate_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDate__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDate_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDate__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDate__0
"SOAP Header for getTransactionsOnDate"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDate__0
"SOAP Body for getTransactionsOnDate"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDate_32_body__sfield__getTransactionsOnDate__0
	"getTransactionsOnDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsOnDate__0;
}
creator.externalinterface
qml_wsdl_import__port__ParaBankServiceImplPort__in__0
"ParaBankServiceImplPort_in"
	annotations = [ "soap:address" =
"http://192.168.1.72:8080/parabank/services/ParaBank";
]
	direction = in;
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsOnDateResponse__0
"SOAP Envelope for getTransactionsOnDateResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDateResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDateResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsOnDateResponse__0
"SOAP Header for getTransactionsOnDateResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsOnDateResponse__0
"SOAP Body for getTransactionsOnDateResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsOnDateResponse_32_body__sfield__getTransactionsOnDateResponse__0
	"getTransactionsOnDateResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsOnDateResponse__0;
}
creator.externalinterface
qml_wsdl_import__port__ParaBankServiceImplPort__out__0
"ParaBankServiceImplPort_out"
	annotations = [ "soap:address" =
"http://192.168.1.72:8080/parabank/services/ParaBank";
]
	direction = out;
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_ParaBankServiceException__0
"SOAP Envelope for ParaBankServiceException"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_ParaBankServiceException__0
		optional;
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_ParaBankServiceException__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_ParaBankServiceException__0
"SOAP Header for ParaBankServiceException"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_ParaBankServiceException__0
"SOAP Body for ParaBankServiceException"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_body__sfield__Fault__0 "Fault"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Fault_32_for_32_ParaBankServiceException__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Fault_32_for_32_ParaBankServiceException__0
"SOAP Fault for ParaBankServiceException"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.primitivefield
	qml_wsdl_import__ParaBankServiceException_32_body__pfield__faultcode__0
	"faultcode"
		type = String;
	creator.primitivefield
	qml_wsdl_import__ParaBankServiceException_32_body__pfield__faultstring__0
	"faultstring"
		type = String;
	creator.primitivefield
	qml_wsdl_import__ParaBankServiceException_32_body__pfield__faultactor__0
	"faultactor"
		type = String
		optional;
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_body__sfield__detail__0 "detail"
		type =
	qml_wsdl_import__seq__SOAP_32_Fault_32_Detail_32_for_32_ParaBankServiceException__0
		optional;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Fault_32_Detail_32_for_32_ParaBankServiceException__0
"SOAP Fault Detail for ParaBankServiceException"
{
	creator.structuredfield
	qml_wsdl_import__ParaBankServiceException_32_fault__sfield__ParaBankServiceException__0
	"ParaBankServiceException"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__ParaBankServiceException__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_initializeDB__0
"SOAP Envelope for initializeDB"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__initializeDB_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDB__0
		optional;
	creator.structuredfield
	qml_wsdl_import__initializeDB_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDB__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDB__0
"SOAP Header for initializeDB"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDB__0
"SOAP Body for initializeDB"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__initializeDB_32_body__sfield__initializeDB__0 "initializeDB"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__initializeDB__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_initializeDBResponse__0
"SOAP Envelope for initializeDBResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__initializeDBResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDBResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__initializeDBResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDBResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_initializeDBResponse__0
"SOAP Header for initializeDBResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_initializeDBResponse__0
"SOAP Body for initializeDBResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__initializeDBResponse_32_body__sfield__initializeDBResponse__0
	"initializeDBResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__initializeDBResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionHistory__0
"SOAP Envelope for getPositionHistory"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistory_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistory__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionHistory_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistory__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistory__0
"SOAP Header for getPositionHistory"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistory__0
"SOAP Body for getPositionHistory"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistory_32_body__sfield__getPositionHistory__0
	"getPositionHistory"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionHistory__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionHistoryResponse__0
"SOAP Envelope for getPositionHistoryResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistoryResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistoryResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionHistoryResponse__0
"SOAP Header for getPositionHistoryResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionHistoryResponse__0
"SOAP Body for getPositionHistoryResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionHistoryResponse_32_body__sfield__getPositionHistoryResponse__0
	"getPositionHistoryResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionHistoryResponse__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_transfer__0
"SOAP Envelope for transfer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__transfer_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transfer__0
		optional;
	creator.structuredfield qml_wsdl_import__transfer_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transfer__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transfer__0
"SOAP Header for transfer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transfer__0
"SOAP Body for transfer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__transfer_32_body__sfield__transfer__0 "transfer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transfer__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_transferResponse__0
"SOAP Envelope for transferResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__transferResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transferResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__transferResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transferResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_transferResponse__0
"SOAP Header for transferResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_transferResponse__0
"SOAP Body for transferResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__transferResponse_32_body__sfield__transferResponse__0
	"transferResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__transferResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransaction__0
"SOAP Envelope for getTransaction"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransaction_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransaction__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransaction_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransaction__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransaction__0
"SOAP Header for getTransaction"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransaction__0
"SOAP Body for getTransaction"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransaction_32_body__sfield__getTransaction__0
	"getTransaction"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransaction__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionResponse__0
"SOAP Envelope for getTransactionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionResponse__0
"SOAP Header for getTransactionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionResponse__0
"SOAP Body for getTransactionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionResponse_32_body__sfield__getTransactionResponse__0
	"getTransactionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_shutdownJmsListener__0
"SOAP Envelope for shutdownJmsListener"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListener_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListener__0
		optional;
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListener_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListener__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListener__0
"SOAP Header for shutdownJmsListener"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListener__0
"SOAP Body for shutdownJmsListener"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListener_32_body__sfield__shutdownJmsListener__0
	"shutdownJmsListener"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__shutdownJmsListener__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_shutdownJmsListenerResponse__0
"SOAP Envelope for shutdownJmsListenerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListenerResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListenerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListenerResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListenerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_shutdownJmsListenerResponse__0
"SOAP Header for shutdownJmsListenerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_shutdownJmsListenerResponse__0
"SOAP Body for shutdownJmsListenerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__shutdownJmsListenerResponse_32_body__sfield__shutdownJmsListenerResponse__0
	"shutdownJmsListenerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__shutdownJmsListenerResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getCustomer__0
"SOAP Envelope for getCustomer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getCustomer_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomer__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getCustomer_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomer__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomer__0
"SOAP Header for getCustomer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomer__0
"SOAP Body for getCustomer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getCustomer_32_body__sfield__getCustomer__0 "getCustomer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getCustomer__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getCustomerResponse__0
"SOAP Envelope for getCustomerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getCustomerResponse__0
"SOAP Header for getCustomerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getCustomerResponse__0
"SOAP Body for getCustomerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getCustomerResponse_32_body__sfield__getCustomerResponse__0
	"getCustomerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getCustomerResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_updateCustomer__0
"SOAP Envelope for updateCustomer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__updateCustomer_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomer__0
		optional;
	creator.structuredfield
	qml_wsdl_import__updateCustomer_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomer__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomer__0
"SOAP Header for updateCustomer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomer__0
"SOAP Body for updateCustomer"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__updateCustomer_32_body__sfield__updateCustomer__0
	"updateCustomer"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__updateCustomer__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_updateCustomerResponse__0
"SOAP Envelope for updateCustomerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__updateCustomerResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__updateCustomerResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_updateCustomerResponse__0
"SOAP Header for updateCustomerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_updateCustomerResponse__0
"SOAP Body for updateCustomerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__updateCustomerResponse_32_body__sfield__updateCustomerResponse__0
	"updateCustomerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__updateCustomerResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPosition__0
"SOAP Envelope for getPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPosition_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPosition__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPosition_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPosition__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPosition__0
"SOAP Header for getPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPosition__0
"SOAP Body for getPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getPosition_32_body__sfield__getPosition__0 "getPosition"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPosition__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionResponse__0
"SOAP Envelope for getPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionResponse__0
"SOAP Header for getPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionResponse__0
"SOAP Body for getPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionResponse_32_body__sfield__getPositionResponse__0
	"getPositionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_sellPosition__0
"SOAP Envelope for sellPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__sellPosition_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPosition__0
		optional;
	creator.structuredfield
	qml_wsdl_import__sellPosition_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPosition__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPosition__0
"SOAP Header for sellPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPosition__0
"SOAP Body for sellPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__sellPosition_32_body__sfield__sellPosition__0 "sellPosition"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__sellPosition__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_sellPositionResponse__0
"SOAP Envelope for sellPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPositionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPositionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_sellPositionResponse__0
"SOAP Header for sellPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_sellPositionResponse__0
"SOAP Body for sellPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__sellPositionResponse_32_body__sfield__sellPositionResponse__0
	"sellPositionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__sellPositionResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_createAccount__0
"SOAP Envelope for createAccount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__createAccount_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccount__0
		optional;
	creator.structuredfield
	qml_wsdl_import__createAccount_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccount__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccount__0
"SOAP Header for createAccount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccount__0
"SOAP Body for createAccount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__createAccount_32_body__sfield__createAccount__0
	"createAccount"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__createAccount__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_createAccountResponse__0
"SOAP Envelope for createAccountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__createAccountResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccountResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__createAccountResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccountResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_createAccountResponse__0
"SOAP Header for createAccountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_createAccountResponse__0
"SOAP Body for createAccountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__createAccountResponse_32_body__sfield__createAccountResponse__0
	"createAccountResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__createAccountResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_startupJmsListener__0
"SOAP Envelope for startupJmsListener"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListener_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListener__0
		optional;
	creator.structuredfield
	qml_wsdl_import__startupJmsListener_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListener__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListener__0
"SOAP Header for startupJmsListener"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListener__0
"SOAP Body for startupJmsListener"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListener_32_body__sfield__startupJmsListener__0
	"startupJmsListener"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__startupJmsListener__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_startupJmsListenerResponse__0
"SOAP Envelope for startupJmsListenerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListenerResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListenerResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__startupJmsListenerResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListenerResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_startupJmsListenerResponse__0
"SOAP Header for startupJmsListenerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_startupJmsListenerResponse__0
"SOAP Body for startupJmsListenerResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__startupJmsListenerResponse_32_body__sfield__startupJmsListenerResponse__0
	"startupJmsListenerResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__startupJmsListenerResponse__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_login__0
"SOAP Envelope for login"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield qml_wsdl_import__login_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_login__0
		optional;
	creator.structuredfield qml_wsdl_import__login_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_login__0;
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Header_32_for_32_login__0
"SOAP Header for login"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_login__0
"SOAP Body for login"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield qml_wsdl_import__login_32_body__sfield__login__0
	"login"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__login__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_loginResponse__0
"SOAP Envelope for loginResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__loginResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_loginResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__loginResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_loginResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_loginResponse__0
"SOAP Header for loginResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_loginResponse__0
"SOAP Body for loginResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__loginResponse_32_body__sfield__loginResponse__0
	"loginResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__loginResponse__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_deposit__0
"SOAP Envelope for deposit"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__deposit_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_deposit__0
		optional;
	creator.structuredfield qml_wsdl_import__deposit_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_deposit__0;
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Header_32_for_32_deposit__0
"SOAP Header for deposit"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_deposit__0
"SOAP Body for deposit"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield qml_wsdl_import__deposit_32_body__sfield__deposit__0
	"deposit"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__deposit__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_depositResponse__0
"SOAP Envelope for depositResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__depositResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_depositResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__depositResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_depositResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_depositResponse__0
"SOAP Header for depositResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_depositResponse__0
"SOAP Body for depositResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__depositResponse_32_body__sfield__depositResponse__0
	"depositResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__depositResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByAmount__0
"SOAP Envelope for getTransactionsByAmount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmount_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmount__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmount_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmount__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmount__0
"SOAP Header for getTransactionsByAmount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmount__0
"SOAP Body for getTransactionsByAmount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmount_32_body__sfield__getTransactionsByAmount__0
	"getTransactionsByAmount"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByAmount__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByAmountResponse__0
"SOAP Envelope for getTransactionsByAmountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmountResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmountResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByAmountResponse__0
"SOAP Header for getTransactionsByAmountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByAmountResponse__0
"SOAP Body for getTransactionsByAmountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByAmountResponse_32_body__sfield__getTransactionsByAmountResponse__0
	"getTransactionsByAmountResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByAmountResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_setParameter__0
"SOAP Envelope for setParameter"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__setParameter_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameter__0
		optional;
	creator.structuredfield
	qml_wsdl_import__setParameter_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameter__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameter__0
"SOAP Header for setParameter"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameter__0
"SOAP Body for setParameter"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__setParameter_32_body__sfield__setParameter__0 "setParameter"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__setParameter__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_setParameterResponse__0
"SOAP Envelope for setParameterResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__setParameterResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameterResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__setParameterResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameterResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_setParameterResponse__0
"SOAP Header for setParameterResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_setParameterResponse__0
"SOAP Body for setParameterResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__setParameterResponse_32_body__sfield__setParameterResponse__0
	"setParameterResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__setParameterResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositions__0
"SOAP Envelope for getPositions"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositions_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositions__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositions_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositions__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositions__0
"SOAP Header for getPositions"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositions__0
"SOAP Body for getPositions"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getPositions_32_body__sfield__getPositions__0 "getPositions"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositions__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getPositionsResponse__0
"SOAP Envelope for getPositionsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionsResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionsResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getPositionsResponse__0
"SOAP Header for getPositionsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getPositionsResponse__0
"SOAP Body for getPositionsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getPositionsResponse_32_body__sfield__getPositionsResponse__0
	"getPositionsResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getPositionsResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByMonthAndType__0
"SOAP Envelope for getTransactionsByMonthAndType"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndType_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndType__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndType_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndType__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndType__0
"SOAP Header for getTransactionsByMonthAndType"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndType__0
"SOAP Body for getTransactionsByMonthAndType"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndType_32_body__sfield__getTransactionsByMonthAndType__0
	"getTransactionsByMonthAndType"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByMonthAndType__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByMonthAndTypeResponse__0
"SOAP Envelope for getTransactionsByMonthAndTypeResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndTypeResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndTypeResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByMonthAndTypeResponse__0
"SOAP Header for getTransactionsByMonthAndTypeResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByMonthAndTypeResponse__0
"SOAP Body for getTransactionsByMonthAndTypeResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByMonthAndTypeResponse_32_body__sfield__getTransactionsByMonthAndTypeResponse__0
	"getTransactionsByMonthAndTypeResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByMonthAndTypeResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactions__0
"SOAP Envelope for getTransactions"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactions_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactions__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactions_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactions__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactions__0
"SOAP Header for getTransactions"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactions__0
"SOAP Body for getTransactions"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactions_32_body__sfield__getTransactions__0
	"getTransactions"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactions__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsResponse__0
"SOAP Envelope for getTransactionsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsResponse__0
"SOAP Header for getTransactionsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsResponse__0
"SOAP Body for getTransactionsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsResponse_32_body__sfield__getTransactionsResponse__0
	"getTransactionsResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByToFromDate__0
"SOAP Envelope for getTransactionsByToFromDate"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDate_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDate__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDate_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDate__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDate__0
"SOAP Header for getTransactionsByToFromDate"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDate__0
"SOAP Body for getTransactionsByToFromDate"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDate_32_body__sfield__getTransactionsByToFromDate__0
	"getTransactionsByToFromDate"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByToFromDate__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getTransactionsByToFromDateResponse__0
"SOAP Envelope for getTransactionsByToFromDateResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse_32_message__sfield__Header__0
	"Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDateResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type =
	qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDateResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getTransactionsByToFromDateResponse__0
"SOAP Header for getTransactionsByToFromDateResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getTransactionsByToFromDateResponse__0
"SOAP Body for getTransactionsByToFromDateResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getTransactionsByToFromDateResponse_32_body__sfield__getTransactionsByToFromDateResponse__0
	"getTransactionsByToFromDateResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getTransactionsByToFromDateResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_requestLoan__0
"SOAP Envelope for requestLoan"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__requestLoan_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoan__0
		optional;
	creator.structuredfield
	qml_wsdl_import__requestLoan_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoan__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoan__0
"SOAP Header for requestLoan"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoan__0
"SOAP Body for requestLoan"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__requestLoan_32_body__sfield__requestLoan__0 "requestLoan"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__requestLoan__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_requestLoanResponse__0
"SOAP Envelope for requestLoanResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoanResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoanResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_requestLoanResponse__0
"SOAP Header for requestLoanResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_requestLoanResponse__0
"SOAP Body for requestLoanResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__requestLoanResponse_32_body__sfield__requestLoanResponse__0
	"requestLoanResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__requestLoanResponse__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_withdraw__0
"SOAP Envelope for withdraw"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__withdraw_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdraw__0
		optional;
	creator.structuredfield qml_wsdl_import__withdraw_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdraw__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdraw__0
"SOAP Header for withdraw"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdraw__0
"SOAP Body for withdraw"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__withdraw_32_body__sfield__withdraw__0 "withdraw"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__withdraw__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_withdrawResponse__0
"SOAP Envelope for withdrawResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__withdrawResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdrawResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__withdrawResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdrawResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_withdrawResponse__0
"SOAP Header for withdrawResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_withdrawResponse__0
"SOAP Body for withdrawResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__withdrawResponse_32_body__sfield__withdrawResponse__0
	"withdrawResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__withdrawResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccounts__0
"SOAP Envelope for getAccounts"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccounts_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccounts__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccounts_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccounts__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccounts__0
"SOAP Header for getAccounts"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccounts__0
"SOAP Body for getAccounts"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getAccounts_32_body__sfield__getAccounts__0 "getAccounts"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccounts__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccountsResponse__0
"SOAP Envelope for getAccountsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountsResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountsResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountsResponse__0
"SOAP Header for getAccountsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountsResponse__0
"SOAP Body for getAccountsResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getAccountsResponse_32_body__sfield__getAccountsResponse__0
	"getAccountsResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccountsResponse__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_buyPosition__0
"SOAP Envelope for buyPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__buyPosition_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPosition__0
		optional;
	creator.structuredfield
	qml_wsdl_import__buyPosition_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPosition__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPosition__0
"SOAP Header for buyPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPosition__0
"SOAP Body for buyPosition"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__buyPosition_32_body__sfield__buyPosition__0 "buyPosition"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__buyPosition__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_buyPositionResponse__0
"SOAP Envelope for buyPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPositionResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPositionResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_buyPositionResponse__0
"SOAP Header for buyPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_buyPositionResponse__0
"SOAP Body for buyPositionResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__buyPositionResponse_32_body__sfield__buyPositionResponse__0
	"buyPositionResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__buyPositionResponse__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_cleanDB__0
"SOAP Envelope for cleanDB"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__cleanDB_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDB__0
		optional;
	creator.structuredfield qml_wsdl_import__cleanDB_32_message__sfield__Body__0
	"Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDB__0;
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDB__0
"SOAP Header for cleanDB"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDB__0
"SOAP Body for cleanDB"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield qml_wsdl_import__cleanDB_32_body__sfield__cleanDB__0
	"cleanDB"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__cleanDB__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_cleanDBResponse__0
"SOAP Envelope for cleanDBResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__cleanDBResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDBResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__cleanDBResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDBResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_cleanDBResponse__0
"SOAP Header for cleanDBResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_cleanDBResponse__0
"SOAP Body for cleanDBResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__cleanDBResponse_32_body__sfield__cleanDBResponse__0
	"cleanDBResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__cleanDBResponse__0;
}
creator.message qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccount__0
"SOAP Envelope for getAccount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccount_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccount__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccount_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccount__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccount__0
"SOAP Header for getAccount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccount__0
"SOAP Body for getAccount"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getAccount_32_body__sfield__getAccount__0 "getAccount"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccount__0;
}
creator.message
qml_wsdl_import__msg__SOAP_32_Envelope_32_for_32_getAccountResponse__0
"SOAP Envelope for getAccountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__out__0 ]
{
	creator.structuredfield
	qml_wsdl_import__getAccountResponse_32_message__sfield__Header__0 "Header"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountResponse__0
		optional;
	creator.structuredfield
	qml_wsdl_import__getAccountResponse_32_message__sfield__Body__0 "Body"
		namespace = "http://schemas.xmlsoap.org/soap/envelope/"
		type = qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountResponse__0;
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Header_32_for_32_getAccountResponse__0
"SOAP Header for getAccountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
}
creator.sequencetype
qml_wsdl_import__seq__SOAP_32_Body_32_for_32_getAccountResponse__0
"SOAP Body for getAccountResponse"
	namespace = "http://schemas.xmlsoap.org/soap/envelope/"
{
	creator.structuredfield
	qml_wsdl_import__getAccountResponse_32_body__sfield__getAccountResponse__0
	"getAccountResponse"
		namespace = "http://service.parabank.parasoft.com/"
		type = qml_wsdl_import__seq__getAccountResponse__0;
}
creator.customaction qmleb036215bc3047e48ed245856d8d1097 "userData"
	interfaces = [ qml_wsdl_import__port__ParaBankServiceImplPort__in__0
	deleted ]
	shortform = "UD"
	direction = in
	tokens = [ reference qmld0af0411b9bb458db286c824ad648744 reference
qml56f394a4a0c74cff9a1ac85a2886ce34 reference
qml56a9315402ee411fabe48b47b2128d9f reference
qml96b164ca9d3547fc879401f07bb6cabf reference
qmleb800fdc51774458bc80029203411801 reference
qml77e0118e70ef418eadbad5ee9f93cb9a reference
qml22cc6a8edfcd4f8489d513f0998a8097 reference
qmld60894140fd14247abcaf5da4af889e1 reference
qmlff0a505a3c0d44709bcbeb336fb8783c reference
qml76e88e1446374c06bce3a66eee82cbff reference
qml05f1ebb755484c37b884c7868193419f reference
qmlb62025778e49400982d17c83c5ed7a1e reference
qmlc03c175efc3447e69614f840d000c2a1 reference
qmled5262b96c834d3f8999043c0d5be104 reference
qml505f35ca16e4439b853cab47af445480 reference
qml06f0b985e149470bacd98d2f0ee8a6c4 reference
qml2c7c4d35ed944c61bab5361aabee7d3b reference
qml17712e511311447d83a7465125bc6b1d reference
qml09d1ec2f19e3414fb8b3ac7f2e98a3e4 reference
qml35221e97c57540369841be38fb137f46 reference
qml91e4bae52d22475987adf62a16801d3e reference
qmld213929032f64782b4ccc1eb879f8a4e reference
qml99303980b292441397a4e28704d570d5 reference
qml472572572b8f4b43b5d61ceb27a3ea27 reference
qmle9938311f4c14bd585ee12c0d9fec6be reference
qml4d4f3cd70ad845ecb070323860167d93 reference
qml9b6567cf9df64de39563e612f431db48 reference
qmlf630e16d1235490e8e3ff91d1dad0e4c reference
qmle36e92872f96472c8c46ccb31d80d9ab reference
qml57a16e83b139454392dcae4da24db4ee reference
qmla19e8962e283498ba43a0e25aaa8aecf ]
	deleted
{
	creator.primitivefield qml91a798a6e6ef41acb04f446aecab6d75 "Flow"
		type = String
		deleted;
	creator.primitivefield qmld0af0411b9bb458db286c824ad648744 "username"
		type = String
		deleted;
	creator.primitivefield qml56f394a4a0c74cff9a1ac85a2886ce34 "password"
		type = String
		deleted;
	creator.primitivefield qml56a9315402ee411fabe48b47b2128d9f
	"getAccount_accountId"
		type = number
		deleted;
	creator.primitivefield qml96b164ca9d3547fc879401f07bb6cabf
	"transfer_fromAccountId"
		type = String
		deleted;
	creator.primitivefield qmleb800fdc51774458bc80029203411801
	"transfer_toAccountId"
		type = number
		deleted;
	creator.primitivefield qml77e0118e70ef418eadbad5ee9f93cb9a "transfer_amount"
		type = String
		deleted;
	creator.primitivefield qmle38cec751add4b9782d5ff64f33f347c
	"transferResponse_transferReturn"
		type = String
		deleted;
	creator.primitivefield qml22cc6a8edfcd4f8489d513f0998a8097
	"getAccounts_customerId"
		type = number
		deleted;
	creator.primitivefield qmld60894140fd14247abcaf5da4af889e1
	"requestLoan_customerId"
		type = String
		deleted;
	creator.primitivefield qmlff0a505a3c0d44709bcbeb336fb8783c
	"requestLoan_amount"
		type = String
		deleted;
	creator.primitivefield qml76e88e1446374c06bce3a66eee82cbff
	"requestLoan_downpayment"
		type = String
		deleted;
	creator.primitivefield qml05f1ebb755484c37b884c7868193419f
	"requestLoan_fromAccountId"
		type = String
		deleted;
	creator.primitivefield qmlb62025778e49400982d17c83c5ed7a1e
	"updateCustomer_customerId"
		type = String
		deleted;
	creator.primitivefield qmlc03c175efc3447e69614f840d000c2a1
	"updateCustomer_firstName"
		type = String
		deleted;
	creator.primitivefield qmled5262b96c834d3f8999043c0d5be104
	"updateCustomer_lastName"
		type = String
		deleted;
	creator.primitivefield qml505f35ca16e4439b853cab47af445480
	"updateCustomer_street"
		type = String
		deleted;
	creator.primitivefield qml06f0b985e149470bacd98d2f0ee8a6c4
	"updateCustomer_city"
		type = String
		deleted;
	creator.primitivefield qml2c7c4d35ed944c61bab5361aabee7d3b
	"updateCustomer_state"
		type = String
		deleted;
	creator.primitivefield qml17712e511311447d83a7465125bc6b1d
	"updateCustomer_zipcode"
		type = String
		deleted;
	creator.primitivefield qml09d1ec2f19e3414fb8b3ac7f2e98a3e4
	"updateCustomer_phoneNumber"
		type = String
		deleted;
	creator.primitivefield qml35221e97c57540369841be38fb137f46
	"updateCustomer_ssn"
		type = String
		deleted;
	creator.primitivefield qml91e4bae52d22475987adf62a16801d3e
	"updateCustomer_username"
		type = String
		deleted;
	creator.primitivefield qmld213929032f64782b4ccc1eb879f8a4e
	"updateCustomer_password"
		type = String
		deleted;
	creator.primitivefield qml99303980b292441397a4e28704d570d5
	"getCustomer_customerId"
		type = String
		deleted;
	creator.primitivefield qml472572572b8f4b43b5d61ceb27a3ea27
	"getTransacton_TransactionId"
		type = number
		deleted;
	creator.primitivefield qmle9938311f4c14bd585ee12c0d9fec6be
	"getTransactionByAmount_accountId"
		type = number
		deleted;
	creator.primitivefield qml4d4f3cd70ad845ecb070323860167d93
	"getTransactionByAmount_amount"
		type = String
		deleted;
	creator.primitivefield qml9b6567cf9df64de39563e612f431db48
	"getTransactionByToFromDate_accountId"
		type = number
		deleted;
	creator.primitivefield qmlf630e16d1235490e8e3ff91d1dad0e4c
	"getTransactionByToFromDate_fromDate"
		type = String
		deleted;
	creator.primitivefield qmle36e92872f96472c8c46ccb31d80d9ab
	"getTransactionByToFromDate_toDate"
		type = String
		deleted;
	creator.primitivefield qml57a16e83b139454392dcae4da24db4ee
	"getTransactionsOnDate_accountId"
		type = number
		deleted;
	creator.primitivefield qmla19e8962e283498ba43a0e25aaa8aecf
	"getTransactionsOnDate_onDate"
		type = String
		deleted;
}
creator.customaction qml4dca3b9dff8a4b45af71a9d0e8f80d36 "userData"
	interfaces = [ qml54c51396e0204384baf5020e8b0b1c08 ]
	shortform = "UD"
	direction = in
	tokens = [ literal "Initialize Parabank application" ]
{
	creator.primitivefield qml802eae1614c24476923c0c79a47480b0 "Flow"
		type = String;
	creator.primitivefield qml1a570d14947b44b2bd76c4d30fad8db3 "username"
		type = String;
	creator.primitivefield qml428a04a9e71f47b393ee1d0607d5d0b2 "password"
		type = String;
	creator.primitivefield qmlb84fb313d57d4652992cf91ffa2d4763
	"getAccount_accountId"
		type = number;
	creator.primitivefield qmle17d643938c34bf9af9e01928e13b19e
	"transfer_fromAccountId"
		type = String;
	creator.primitivefield qml4da9ba9dd61c4018afb2a0a3c844ddaa
	"transfer_toAccountId"
		type = number;
	creator.primitivefield qml44b99d54131c4168a96783377887dc88 "transfer_amount"
		type = String;
	creator.primitivefield qmla2d9ef90e43d468197d98ba52b069140
	"transferResponse_transferReturn"
		type = String
		deleted;
	creator.primitivefield qml80f49ed9b14b426e8876cb95270a7f6f
	"getAccounts_customerId"
		type = number;
	creator.primitivefield qml3b4efc3f71de49a482149858ead20175
	"requestLoan_customerId"
		type = String;
	creator.primitivefield qml57bedcf48840475695df9e4832503cdb
	"requestLoan_amount"
		type = String;
	creator.primitivefield qml7dc79f5e05a94d8e9f77d445f27e6465
	"requestLoan_downpayment"
		type = String;
	creator.primitivefield qml953609c977e3459483904b6a133eed99
	"requestLoan_fromAccountId"
		type = String;
	creator.primitivefield qml36cd54d7385d4525b2e1516f850241a4
	"updateCustomer_customerId"
		type = String;
	creator.primitivefield qml4184ae7ef7fe41fabdc1095b3c568222
	"updateCustomer_firstName"
		type = String
		deleted;
	creator.primitivefield qml51414537f9b84d73957f5be721e6c4a9
	"updateCustomer_lastName"
		type = String
		deleted;
	creator.primitivefield qml4b43e0d2d5074ee5be2abce0c575c039
	"updateCustomer_street"
		type = String
		deleted;
	creator.primitivefield qml437a947e8dcd474183bbb51e12c96d4f
	"updateCustomer_city"
		type = String
		deleted;
	creator.primitivefield qml495c4908ceb34ba7bdffa54b605faffb
	"updateCustomer_state"
		type = String
		deleted;
	creator.primitivefield qmlb825652b4c30402bbbe20ed51c82c559
	"updateCustomer_zipcode"
		type = String
		deleted;
	creator.primitivefield qml46afa3db40bc4f36acf942b5e27f3da8
	"updateCustomer_phoneNumber"
		type = String
		deleted;
	creator.primitivefield qml9b8bd0aaee5f4178947fd02cdc2b336b
	"updateCustomer_ssn"
		type = String
		deleted;
	creator.primitivefield qml6ff0a42f8fec444eb7240e4dbe61e9fe
	"updateCustomer_username"
		type = String
		deleted;
	creator.primitivefield qml3975580f648a48eaa8e86aa9b38a2923
	"updateCustomer_password"
		type = String
		deleted;
	creator.primitivefield qmle706e1fac2fa4ea988a70213d2cce4fd
	"getCustomer_customerId"
		type = String;
	creator.primitivefield qmle46c978673f142969358716ee37a915c
	"getTransacton_TransactionId"
		type = number;
	creator.primitivefield qml084fa0b31d554f8db7123dc049c9800e
	"getTransactionByAmount_accountId"
		type = number;
	creator.primitivefield qml62f3c9467b2c495fa21c1be9a826cd09
	"getTransactionByAmount_amount"
		type = String;
	creator.primitivefield qml9a94abe80d22433a891aac034a3fff67
	"getTransactionByToFromDate_accountId"
		type = number;
	creator.primitivefield qml0b1c780fd51c4cbc8f1b27496bdcbac0
	"getTransactionByToFromDate_fromDate"
		type = String;
	creator.primitivefield qml3acf48daae5641319d024e99d9aae5c1
	"getTransactionByToFromDate_toDate"
		type = String;
	creator.primitivefield qml2cfee40f05ea4ef3ad06bc77f3087ccf
	"getTransactionsOnDate_accountId"
		type = number;
	creator.primitivefield qml61cc984b30c643628a001435e52d6fe9
	"getTransactionsOnDate_onDate"
		type = String;
	creator.primitivefield qmlfcf4b95ce3394a1383ae0e120069db7d
	"createAccount_customerId"
		type = String;
	creator.primitivefield qml8651989729cc40baa1921420b706a567
	"createAccount_newAccountType"
		type = number;
	creator.primitivefield qmld4c60efe956e44c5af6c5f87d31f587a
	"createAccount_fromAccountId"
		type = number;
}
creator.externalinterface qml54c51396e0204384baf5020e8b0b1c08 "user data"
	direction = in;
creator.message qml3e0246d090114f34af9f7eea161ce269 "Raw XML"
	interfaces = [ qml54c51396e0204384baf5020e8b0b1c08 ]
{
	creator.primitivefield qml47f2047d7c1749cf94be1fc7a6a3a6d5 "data"
		type = String;
}