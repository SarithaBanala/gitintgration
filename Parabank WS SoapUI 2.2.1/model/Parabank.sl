com.conformiq.creator.structure.v15
creator.customaction qml4dca3b9dff8a4b45af71a9d0e8f80d36 "userData"
	interfaces = [ qml54c51396e0204384baf5020e8b0b1c08 ]
	shortform = "UD"
	direction = in
	tokens = [ literal "Initialize Parabank application" ]
{
	creator.primitivefield qml802eae1614c24476923c0c79a47480b0 "Flow"
		type = String;
	creator.primitivefield qml1a570d14947b44b2bd76c4d30fad8db3 "username"
		type = String;
	creator.primitivefield qml428a04a9e71f47b393ee1d0607d5d0b2 "password"
		type = String;
	creator.primitivefield qmlb84fb313d57d4652992cf91ffa2d4763
	"getAccount_accountId"
		type = number;
	creator.primitivefield qmle17d643938c34bf9af9e01928e13b19e
	"transfer_fromAccountId"
		type = String;
	creator.primitivefield qml4da9ba9dd61c4018afb2a0a3c844ddaa
	"transfer_toAccountId"
		type = number;
	creator.primitivefield qml44b99d54131c4168a96783377887dc88 "transfer_amount"
		type = String;
	creator.primitivefield qmla2d9ef90e43d468197d98ba52b069140
	"transferResponse_transferReturn"
		type = String
		deleted;
	creator.primitivefield qml80f49ed9b14b426e8876cb95270a7f6f
	"getAccounts_customerId"
		type = number;
	creator.primitivefield qml3b4efc3f71de49a482149858ead20175
	"requestLoan_customerId"
		type = String;
	creator.primitivefield qml57bedcf48840475695df9e4832503cdb
	"requestLoan_amount"
		type = String;
	creator.primitivefield qml7dc79f5e05a94d8e9f77d445f27e6465
	"requestLoan_downpayment"
		type = String;
	creator.primitivefield qml953609c977e3459483904b6a133eed99
	"requestLoan_fromAccountId"
		type = String;
	creator.primitivefield qml36cd54d7385d4525b2e1516f850241a4
	"updateCustomer_customerId"
		type = String;
	creator.primitivefield qml4184ae7ef7fe41fabdc1095b3c568222
	"updateCustomer_firstName"
		type = String
		deleted;
	creator.primitivefield qml51414537f9b84d73957f5be721e6c4a9
	"updateCustomer_lastName"
		type = String
		deleted;
	creator.primitivefield qml4b43e0d2d5074ee5be2abce0c575c039
	"updateCustomer_street"
		type = String
		deleted;
	creator.primitivefield qml437a947e8dcd474183bbb51e12c96d4f
	"updateCustomer_city"
		type = String
		deleted;
	creator.primitivefield qml495c4908ceb34ba7bdffa54b605faffb
	"updateCustomer_state"
		type = String
		deleted;
	creator.primitivefield qmlb825652b4c30402bbbe20ed51c82c559
	"updateCustomer_zipcode"
		type = String
		deleted;
	creator.primitivefield qml46afa3db40bc4f36acf942b5e27f3da8
	"updateCustomer_phoneNumber"
		type = String
		deleted;
	creator.primitivefield qml9b8bd0aaee5f4178947fd02cdc2b336b
	"updateCustomer_ssn"
		type = String
		deleted;
	creator.primitivefield qml6ff0a42f8fec444eb7240e4dbe61e9fe
	"updateCustomer_username"
		type = String
		deleted;
	creator.primitivefield qml3975580f648a48eaa8e86aa9b38a2923
	"updateCustomer_password"
		type = String
		deleted;
	creator.primitivefield qmle706e1fac2fa4ea988a70213d2cce4fd
	"getCustomer_customerId"
		type = String;
	creator.primitivefield qmle46c978673f142969358716ee37a915c
	"getTransacton_TransactionId"
		type = number;
	creator.primitivefield qml084fa0b31d554f8db7123dc049c9800e
	"getTransactionByAmount_accountId"
		type = number;
	creator.primitivefield qml62f3c9467b2c495fa21c1be9a826cd09
	"getTransactionByAmount_amount"
		type = String;
	creator.primitivefield qml9a94abe80d22433a891aac034a3fff67
	"getTransactionByToFromDate_accountId"
		type = number;
	creator.primitivefield qml0b1c780fd51c4cbc8f1b27496bdcbac0
	"getTransactionByToFromDate_fromDate"
		type = String;
	creator.primitivefield qml3acf48daae5641319d024e99d9aae5c1
	"getTransactionByToFromDate_toDate"
		type = String;
	creator.primitivefield qml2cfee40f05ea4ef3ad06bc77f3087ccf
	"getTransactionsOnDate_accountId"
		type = number;
	creator.primitivefield qml61cc984b30c643628a001435e52d6fe9
	"getTransactionsOnDate_onDate"
		type = String;
	creator.primitivefield qmlfcf4b95ce3394a1383ae0e120069db7d
	"createAccount_customerId"
		type = String;
	creator.primitivefield qml8651989729cc40baa1921420b706a567
	"createAccount_newAccountType"
		type = number;
	creator.primitivefield qmld4c60efe956e44c5af6c5f87d31f587a
	"createAccount_fromAccountId"
		type = number;
}
creator.externalinterface qml54c51396e0204384baf5020e8b0b1c08 "user data"
	direction = in;
creator.message qml3e0246d090114f34af9f7eea161ce269 "Raw XML"
	interfaces = [ qml54c51396e0204384baf5020e8b0b1c08 ]
{
	creator.primitivefield qml47f2047d7c1749cf94be1fc7a6a3a6d5 "data"
		type = String;
}
