SoapUIBridge - Conformiq test suite converter for SoapUI

Copyright (c) 2016 Conformiq Inc. and its subsidiaries.  SoapUIBridge is made
available under the terms of the Eclipse Public License v1.0 which accompanies
this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html.

SoapUIBridge depends on, and includes an unmodified copy of SoapUI Open Source
v5.2.1 (http://www.soapui.org/), which is copyright (C) 2004-2014
smartbear.com, and distributed under the European Union Public License (EUPL)
V. 1.1.  The SoapUI license, and the licenses of its supporting components,
are available in the 3rdparty-licenses subdirectory in the SoapUIBridge
distribution.

Remote execution of this SoapUIBridge utility from Conformiq Creator also requires 
_significant amounts of main memory_. Close all other memory intensive 
applications before to avoid failure to start SoapUIBridge due to lack of memory!

We highly recommend to use a computer with more than 8GB of main memory to run
 SoapUIBridge remotely (with 8GB memory test suite conversion for even smaller 
test suites can easily require about 20 min).

Standalone usage from command line (Java 1.7 or newer required):
    java -jar <path to SoapUIBridge>SoapUIBridge.jar --generate [INFILE] [OUTDIR] [PROJECT] [WSDL URL]
    java -jar <path to SoapUIBridge>SoapUIBridge.jar --execute [PROJECT] [SUITEID] [CASEID] [STEPID]
    java -jar <path to SoapUIBridge>SoapUIBridge.jar --help
    

With --generate (alternatively, -g), SoapUIBridge will create a SoapUI project
from a test suite exported from Conformiq Creator using the SerializerScripter
scripting back-end.  Here,
    [INFILE]   is the name of a SerializerScripter scripting back-end output
               file,
    [OUTDIR]   is the name of the directory in which to save output files,
    [PROJECT]  is a name for the SoapUI 5.2.1 project (the output file will be 
               saved to [PROJECT].xml in [OUTDIR]), and
    [WSDL URL] is the web URL or location where the WSDL file with message 
               definitions can be retrieved.

In the --execute (-e) mode, SoapUIBridge will execute a test step from a test
case in a SoapUI project.  Here,
    [PROJECT]  is the name of a SoapUI project file,
    [SUITEID]  is the name of a test suite in the SoapUI project
    [CASEID]   is the numeric identifier, or the name of a test case in the
    	         test suite, and
    [STEPID]   is the numeric identifier, or the name of a test step in the
               test case.
 