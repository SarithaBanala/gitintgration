/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.value.CreatorListValue;
import com.conformiq.creator.domain.generic.value.CreatorValue;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorListValueImpl implements CreatorListValue, Serializable {

    private static final long serialVersionUID = 5150539448650430617L;
    private final List<CreatorValueWrapper<CreatorValue>> items;
    private final CreatorGenericType type;

    public SerializableCreatorListValueImpl(CreatorListValue value) {
        this.items = new ArrayList<>(value.getContents().size());
        for (CreatorValueWrapper<CreatorValue> item : value.getContents()) {
            this.items.add(new SerializableCreatorValueWrapperImpl(item));
        }
        this.type = new SerializableCreatorGenericTypeImpl(value.getType());
    }

    @Override
    public CreatorGenericType getType() {
        return type;
    }

    @Override
    public List<CreatorValueWrapper<CreatorValue>> getContents() {
        return items;
    }

}
