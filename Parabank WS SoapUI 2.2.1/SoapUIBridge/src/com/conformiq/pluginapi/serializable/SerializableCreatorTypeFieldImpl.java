/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.CreatorTypeField;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorTypeFieldImpl implements CreatorTypeField, Serializable {

    private static final long serialVersionUID = 1381997958805015437L;
    private final String namaspace;
    private boolean isOptional;

    public SerializableCreatorTypeFieldImpl(CreatorTypeField type) {
        this.namaspace = type.getNamespace();
        this.isOptional = type.isOptional();
        // TODO Auto-generated constructor stub
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getNamespace() {
        return this.namaspace;
    }

    @Override
    public CreatorGenericType getType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }
}
