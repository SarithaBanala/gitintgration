/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.generic.value.CreatorValue;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorValueWrapperImpl implements CreatorValueWrapper<CreatorValue>, Serializable {

    private static final long serialVersionUID = -7225378225077193350L;
    private boolean isDontCare;
    private CreatorValue value;

    public SerializableCreatorValueWrapperImpl(CreatorValueWrapper<CreatorValue> value) {
        if (value.isDontCare()) {
            this.isDontCare = true;
            this.value = null;
        } else {
            this.isDontCare = false;
            this.value = SerializableCreatorValueFactory.create(value.get());
        }
    }

    public SerializableCreatorValueWrapperImpl(CreatorValue value) {
        this.isDontCare = false;
        this.value = value;
    }

    @Override
    public boolean isDontCare() {
        return isDontCare;
    }

    @Override
    public CreatorValue get() {
        return value;
    }

}
