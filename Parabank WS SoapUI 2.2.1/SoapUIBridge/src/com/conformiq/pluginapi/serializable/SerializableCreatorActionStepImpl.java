/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.CreatorAction;
import com.conformiq.creator.CreatorActionStep;
import com.conformiq.creator.domain.generic.CreatorGenericAction;
import com.conformiq.qtronic2.TimeStamp;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorActionStepImpl implements CreatorActionStep, Serializable {

    private static final long serialVersionUID = -1415857884495084273L;
    private final CreatorGenericAction creatorGenericAction;
    private final StepType stepType;

    public SerializableCreatorActionStepImpl(CreatorGenericAction creatorGenericAction, StepType stepType) {
        this.creatorGenericAction = creatorGenericAction;
        this.stepType = stepType;
    }

    @Override
    public StepType getType() {
        return this.stepType;
    }

    @Override
    public TimeStamp getTimeStamp() {
        throw new UnsupportedOperationException();
    }

    @Override
    public CreatorAction getAction() {
        return creatorGenericAction;
    }

}
