/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *     Heikki Tauriainen - add a version header to the generated output
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.conformiq.designer.DesignConfiguration;
import com.conformiq.designer.ProgressNotificationSink;

public class Serializer {

    // The output format used by the Read() and Write() routines consists of serialized Java objects of the following
    // types in this order:
    // (1) a String to identify the output as having been generated using the com.conformiq.pluginapi.serializable API;
    // (2) an int identifying the version of the com.conformiq.plugin.serializable API that was used for generating the
    // output;
    // (3) an int identifying the version of the Conformiq Plugin API to which the design configuration that follows
    // conforms to; and
    // (4) the serialized DesignConfiguration object.
    // These constants define the values used for the output header information (1)-(3).
    private static final String SERIALIZER_MAGIC_STRING = "com.conformiq.pluginapi.serializable";
    private static final int SERIALIZATION_API_VERSION = 1;
    private static final int SCRIPTER_PLUGINAPI_VERSION = 11;

    /**
     * Returns the version of the com.conformiq.pluginapi.serializable API supported by this serializer.
     */
    public static int getSerializationAPIVersion() {
        return SERIALIZATION_API_VERSION;
    }

    /**
     * Returns the version of the Conformiq Plugin API supported by this serializer.
     */
    public static int getScripterPluginAPIVersion() {
        return SCRIPTER_PLUGINAPI_VERSION;
    }

    /**
     * Base class for errors occurring when reading the header of a serialized design configuration.
     */
    public static class HeaderValidationError extends RuntimeException {
        private static final long serialVersionUID = -7443670081199072500L;

        protected HeaderValidationError() {
            super();
        }
    }

    /**
     * Exception thrown when a (supposedly) serialized design configuration fails to identify itself as having been
     * generated using the com.conformiq.pluginapi.serializable API.
     */
    public static class HeaderMismatch extends HeaderValidationError {
        private static final long serialVersionUID = 4825070893889376968L;

        HeaderMismatch() {
            super();
        }
    }

    /**
     * Exception thrown when a serialized design configuration has been generated using an incompatible version of the
     * com.conformiq.pluginapi.serializable API.
     */
    public static class SerializationAPIVersionMismatch extends HeaderValidationError {
        private static final long serialVersionUID = -32615047359235697L;

        SerializationAPIVersionMismatch(final int version) {
            super();
            this.version = version;
        }

        /**
         * Returns the incompatible version.
         * 
         * @return the version number
         */
        public int getOffendingVersionNumber() {
            return version;
        }

        private final int version;
    }

    /**
     * Exception thrown when the Conformiq Plugin API version of a serialized design configuration is not compatible
     * with this version of the com.conformiq.pluginapi.serializable API.
     */
    public static class PluginAPIVersionMismatch extends HeaderValidationError {
        private static final long serialVersionUID = -4350848240200845027L;

        PluginAPIVersionMismatch(final int version) {
            super();
            this.version = version;
        }

        /**
         * Returns the incompatible version.
         * 
         * @return the version number
         */
        public int getOffendingVersionNumber() {
            return version;
        }

        private final int version;
    }

    /**
     * Reads a design configuration (previously serialized using the Write() routine) from an input stream.
     * 
     * @param is
     *            the input stream
     * @return the design configuration
     * @throws IOException
     *             if an error occurs while reading from the stream
     * @throws HeaderValidationError
     *             if the input cannot be recognized as a serialized design configuration (of a compatible version of
     *             the serialization API and the Conformiq Plugin API)
     * @throws ClassNotFoundException
     *             if a design configuration fails to be read successfully from the input stream
     */
    public static DesignConfiguration Read(final InputStream is)
            throws IOException, HeaderValidationError, ClassNotFoundException {
        final String magic_string;
        final int serializer_version;
        final int pluginapi_version;
        final ObjectInputStream ois = new ObjectInputStream(is);
        magic_string = (String)ois.readObject();
        if (!magic_string.equals(SERIALIZER_MAGIC_STRING)) {
            throw new HeaderMismatch();
        }
        serializer_version = (int)ois.readObject();
        if (serializer_version != SERIALIZATION_API_VERSION) {
            throw new SerializationAPIVersionMismatch(serializer_version);
        }
        pluginapi_version = (int)ois.readObject();
        if (pluginapi_version != SCRIPTER_PLUGINAPI_VERSION) {
            throw new PluginAPIVersionMismatch(pluginapi_version);
        }
        return (DesignConfiguration)ois.readObject();
    }

    /**
     * Writes a design configuration (preceded by a version header) to the given output stream.
     * 
     * @param os
     *            the output stream
     * @param dc
     *            the design configuration
     * @param ns
     *            a progress notification sink
     * @throws IOException
     *             if an error occurs while writing to the output stream
     */
    public static void Write(final OutputStream os, final DesignConfiguration dc, final ProgressNotificationSink ns)
            throws IOException {
        final ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(SERIALIZER_MAGIC_STRING);
        oos.writeObject(SERIALIZATION_API_VERSION);
        oos.writeObject(SCRIPTER_PLUGINAPI_VERSION);
        oos.writeObject(new SerializableDesignConfigurationImpl(dc, ns));
        oos.flush();
    }

}
