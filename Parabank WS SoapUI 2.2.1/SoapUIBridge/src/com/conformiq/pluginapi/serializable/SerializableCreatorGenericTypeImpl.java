/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.CreatorDomain;
import com.conformiq.creator.domain.generic.CreatorGenericType;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorGenericTypeImpl implements CreatorGenericType, Serializable {

    private static final long serialVersionUID = -7487237325466938329L;
    private final Type type;
    private final String namespace;
    private final String name;

    public SerializableCreatorGenericTypeImpl(CreatorGenericType type) {
        this.type = type.getType();
        this.namespace = type.getNamespace();
        this.name = type.getName();
    }

    @Override
    public CreatorDomain getDomain() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getIdentifier() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getNamespace() {
        return this.namespace;
    }
}
