/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import com.conformiq.creator.domain.generic.value.CreatorBooleanValue;
import com.conformiq.creator.domain.generic.value.CreatorEnumValue;
import com.conformiq.creator.domain.generic.value.CreatorListValue;
import com.conformiq.creator.domain.generic.value.CreatorMessageValue;
import com.conformiq.creator.domain.generic.value.CreatorNumberValue;
import com.conformiq.creator.domain.generic.value.CreatorSequenceValue;
import com.conformiq.creator.domain.generic.value.CreatorStringValue;
import com.conformiq.creator.domain.generic.value.CreatorStructuredValue;
import com.conformiq.creator.domain.generic.value.CreatorValue;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorValueFactory {

    static CreatorValue create(CreatorValue value) {

        switch (value.getType().getType()) {
        case BOOLEAN:
            return new SerializableCreatorBooleanValueImpl((CreatorBooleanValue)value);
        case CHOICE:
            return new SerializableCreatorStructuredValueImpl((CreatorStructuredValue)value);
        case CUSTOMACTION:
            throw new UnsupportedOperationException();
        case DATE:
            throw new UnsupportedOperationException();
        case ENUMERATION:
            return new SerializableCreatorEnumValueImpl((CreatorEnumValue)value);
        case LIST:
            return new SerializableCreatorListValueImpl((CreatorListValue)value);
        case MESSAGE:
            return new SerializableCreatorMessageValueImpl((CreatorMessageValue)value);
        case NUMBER:
            return new SerializableCreatorNumberValueImpl((CreatorNumberValue)value);
        case SEQUENCE:
            return new SerializableCreatorSequenceValueImpl((CreatorSequenceValue)value);
        case STRING:
            return new SerializableCreatorStringValueImpl((CreatorStringValue)value);
        default:
            break;
        }
        throw new UnsupportedOperationException();
    }

}
