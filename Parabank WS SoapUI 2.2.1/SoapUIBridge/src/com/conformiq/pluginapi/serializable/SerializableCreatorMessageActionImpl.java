/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.CreatorDomain;
import com.conformiq.creator.domain.generic.CreatorExternalInterface;
import com.conformiq.creator.domain.generic.CreatorMessageAction;
import com.conformiq.creator.domain.generic.value.CreatorMessageValue;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorMessageActionImpl implements CreatorMessageAction, Serializable {

    private static final long serialVersionUID = 461403587101152831L;
    private final CreatorDomain creatorDomain;
    private final Kind actionKind;
    private final CreatorMessageValue message;
    private final CreatorExternalInterface interfase;
    private final ActionType actionType;

    public SerializableCreatorMessageActionImpl(CreatorMessageAction creatorMessageAction) {
        this.actionKind = creatorMessageAction.getActionKind();
        this.message = new SerializableCreatorMessageValueImpl(creatorMessageAction.getMessage());
        this.creatorDomain = creatorMessageAction.getActionDomain();
        this.interfase = new SerializableCreatorExternalInterfaceImpl(creatorMessageAction.getInterface());
        this.actionType = creatorMessageAction.getActionType();
    }

    @Override
    public CreatorDomain getActionDomain() {
        return this.creatorDomain;
    }

    @Override
    public ActionType getActionType() {
        return this.actionType;
    }

    @Override
    public Kind getActionKind() {
        return this.actionKind;
    }

    @Override
    public CreatorExternalInterface getInterface() {
        return this.interfase;
    }

    @Override
    public CreatorMessageValue getMessage() {
        return this.message;
    }

}
