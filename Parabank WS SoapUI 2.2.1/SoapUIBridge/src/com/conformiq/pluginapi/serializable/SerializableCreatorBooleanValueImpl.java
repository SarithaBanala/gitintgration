/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.value.CreatorBooleanValue;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorBooleanValueImpl implements CreatorBooleanValue, Serializable {

    private static final long serialVersionUID = 5732958195623301233L;
    private final boolean bulean;
    private final CreatorGenericType type;

    public SerializableCreatorBooleanValueImpl(CreatorBooleanValue value) {
        this.bulean = value.getBoolean();
        this.type = new SerializableCreatorGenericTypeImpl(value.getType());
    }

    @Override
    public CreatorGenericType getType() {
        return this.type;
    }

    @Override
    public boolean getBoolean() {
        return bulean;
    }

}
