/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.conformiq.creator.domain.generic.CreatorExternalInterface;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorExternalInterfaceImpl implements CreatorExternalInterface, Serializable {

    private static final long serialVersionUID = -8691748145747668L;
    private final Map<String, String> anotations;
    private final String name;
    private final Direction direction;

    public SerializableCreatorExternalInterfaceImpl(CreatorExternalInterface interfase) {
        this.anotations = new HashMap<>(interfase.getAnnotations());
        this.name = interfase.getName();
        this.direction = interfase.getDirection();
    }

    @Override
    public Map<String, String> getAnnotations() {
        return this.anotations;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public Direction getDirection() {
        return this.direction;
    }

}
