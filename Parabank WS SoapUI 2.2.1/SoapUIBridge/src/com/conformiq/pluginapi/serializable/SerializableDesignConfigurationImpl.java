/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.conformiq.designer.Checkpoint;
import com.conformiq.designer.CheckpointBase;
import com.conformiq.designer.DesignConfiguration;
import com.conformiq.designer.ProgressNotificationSink;
import com.conformiq.designer.TestCase;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableDesignConfigurationImpl implements DesignConfiguration, Serializable {

    private static final long serialVersionUID = 3367319949972558336L;
    private final List<TestCase> testCases;

    public SerializableDesignConfigurationImpl(DesignConfiguration dc, ProgressNotificationSink ns) {
        testCases = new ArrayList<>(dc.getTestCases().size());
        for (TestCase tc : dc) {
            testCases.add(new SerializableTestCaseImpl(tc, ns));
        }
    }

    @Override
    public Iterator<TestCase> iterator() {
        return testCases.iterator();
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException();

    }

    @Override
    public String getIdentifier() {
        throw new UnsupportedOperationException();

    }

    @Override
    public List<TestCase> getTestCases() {
        throw new UnsupportedOperationException();

    }

    @Override
    public CoverageSetting getCoverageSetting(CheckpointBase checkpoint) {
        throw new UnsupportedOperationException();

    }

    @Override
    public boolean isCoverageSettingInherited(CheckpointBase checkpoint) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Checkpoint> getUncoveredCheckpoints() {
        throw new UnsupportedOperationException();

    }

}
