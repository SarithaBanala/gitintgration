/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.conformiq.creator.CreatorAction;
import com.conformiq.creator.CreatorActionStep;
import com.conformiq.creator.domain.generic.CreatorGenericAction;
import com.conformiq.creator.domain.generic.CreatorMessageAction;
import com.conformiq.designer.Checkpoint;
import com.conformiq.designer.DesignConfiguration;
import com.conformiq.designer.ProgressNotificationSink;
import com.conformiq.designer.TestCase;
import com.conformiq.designer.TestStep;
import com.conformiq.qtronic2.TimeStamp;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableTestCaseImpl implements TestCase, Serializable {

    private static final long serialVersionUID = 6613037495779718614L;
    private final List<TestStep> testSteps;
    private final int id;
    private final String name;

    public SerializableTestCaseImpl(TestCase tc, ProgressNotificationSink ns) {
        this.id = tc.getID();
        this.name = tc.getName();
        testSteps = new LinkedList<>();

        for (TestStep testStep : tc.getTestSteps()) {
            switch (testStep.getType()) {

            case CREATOR_ACTION:
                CreatorActionStep creatorActionStep = (CreatorActionStep)testStep;
                CreatorAction creatorAction = creatorActionStep.getAction();
                switch (creatorAction.getActionDomain()) {
                case GENERIC:
                    CreatorGenericAction creatorGenericAction = (CreatorGenericAction)creatorAction;
                    switch (creatorGenericAction.getActionKind()) {
                    case CUSTOM:
                        break;
                    case MESSAGE:
                        CreatorMessageAction creatorMessageAction = (CreatorMessageAction)creatorGenericAction;
                        SerializableCreatorActionStepImpl step = new SerializableCreatorActionStepImpl(
                                new SerializableCreatorMessageActionImpl(creatorMessageAction), testStep.getType());
                        testSteps.add(step);
                        break;
                    }
                    break;
                case GUI:
                    break;
                }
                break;
            case CHECKPOINT:
            case EXECUTION_INFO:
            case EXTERNAL_MESSAGE:
            case INTERNAL_MESSAGE:
            case SECTION_INFO:
                break;
            }
        }

    }

    @Override
    public Iterator<TestStep> iterator() {
        return testSteps.iterator();
    }

    @Override
    public String getName() {
        return this.name;

    }

    @Override
    public int getID() {
        return this.id;
    }

    @Override
    public int getPriority(DesignConfiguration dc) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPriority() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getProbability() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Checkpoint getMainTarget() throws NoOSIMethodologySupportException {
        throw new UnsupportedOperationException();

    }

    @Override
    public String getDescription() {
        throw new UnsupportedOperationException();

    }

    @Override
    public List<TestStep> getTestSteps() {
        throw new UnsupportedOperationException();

    }

    @Override
    public TimeStamp getEndTime() {
        throw new UnsupportedOperationException();

    }

    @Override
    public List<TestCase> getPrerequisites() throws NoOSIMethodologySupportException {
        throw new UnsupportedOperationException();

    }

    @Override
    public List<TestCase> getDependents() throws NoOSIMethodologySupportException {
        throw new UnsupportedOperationException();

    }

    @Override
    public TestCase getPreconditionTest() {
        throw new UnsupportedOperationException();

    }

}
