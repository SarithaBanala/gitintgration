/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.CreatorTypeField;
import com.conformiq.creator.domain.generic.value.CreatorMessageValue;
import com.conformiq.creator.domain.generic.value.CreatorValueField;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorMessageValueImpl implements CreatorMessageValue, Serializable {

    private static final long serialVersionUID = -1868692958841732865L;
    private final List<CreatorValueField> fields;
    private CreatorGenericType type;

    public SerializableCreatorMessageValueImpl(CreatorMessageValue message) {
        List<CreatorValueField> fields = message.getFields();
        this.fields = new ArrayList<>(fields.size());
        for (CreatorValueField field : fields) {
            this.fields.add(new SerializableCreatorValueFieldImpl(field));
        }
        this.type = new SerializableCreatorGenericTypeImpl(message.getType());
    }

    @Override
    public List<CreatorValueField> getFields() {
        return this.fields;
    }

    @Override
    public CreatorValueField getFieldValue(CreatorTypeField field) {
        throw new UnsupportedOperationException();
    }

    @Override
    public CreatorGenericType getType() {
        return this.type;
    }

}
