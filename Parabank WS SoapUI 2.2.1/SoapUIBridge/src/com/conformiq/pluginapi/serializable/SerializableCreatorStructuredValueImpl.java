/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.CreatorTypeField;
import com.conformiq.creator.domain.generic.value.CreatorStructuredValue;
import com.conformiq.creator.domain.generic.value.CreatorValueField;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorStructuredValueImpl implements CreatorStructuredValue, Serializable {

    private static final long serialVersionUID = 5049922608198611568L;
    private List<Object> fields;

    public SerializableCreatorStructuredValueImpl(CreatorStructuredValue value) {
        this.fields = new ArrayList<>(value.getFields().size());
        for (CreatorValueField field : value.getFields()) {
            fields.add(new SerializableCreatorValueFieldImpl(field));
        }
    }

    @Override
    public CreatorGenericType getType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<CreatorValueField> getFields() {
        throw new UnsupportedOperationException();
    }

    @Override
    public CreatorValueField getFieldValue(CreatorTypeField field) {
        throw new UnsupportedOperationException();
    }

}
