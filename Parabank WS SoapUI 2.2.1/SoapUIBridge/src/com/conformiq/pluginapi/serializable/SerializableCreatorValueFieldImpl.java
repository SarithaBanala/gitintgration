/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.generic.CreatorTypeField;
import com.conformiq.creator.domain.generic.value.CreatorValue;
import com.conformiq.creator.domain.generic.value.CreatorValueField;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorValueFieldImpl implements CreatorValueField, Serializable {

    private static final long serialVersionUID = -2542163625812226179L;
    private final SerializableCreatorValueWrapperImpl value;
    private final String name;
    private final SerializableCreatorTypeFieldImpl type;

    public SerializableCreatorValueFieldImpl(CreatorValueField field) {
        this.value = new SerializableCreatorValueWrapperImpl(field.getValue());
        this.name = field.getName();
        this.type = new SerializableCreatorTypeFieldImpl(field.getType());
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public CreatorValueWrapper<CreatorValue> getValue() {
        return this.value;
    }

    @Override
    public CreatorTypeField getType() {
        return type;
    }
}
