/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. The com.conformiq.pluginapi.serializable package is made
 * available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.pluginapi.serializable;

import java.io.Serializable;

import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.value.CreatorEnumValue;

// NOTE: Whenever making changes to this class, remember to update the version number of the
// com.conformiq.pluginapi.serializable API (in Serializer.java) if the changes are not compatible with older versions.
public class SerializableCreatorEnumValueImpl implements CreatorEnumValue, Serializable {

    private static final long serialVersionUID = -992679010864742288L;
    private final String value;
    private final CreatorGenericType type;

    public SerializableCreatorEnumValueImpl(CreatorEnumValue value2) {
        this.value = value2.getValue();
        this.type = new SerializableCreatorGenericTypeImpl(value2.getType());
    }

    @Override
    public CreatorGenericType getType() {
        return type;
    }

    @Override
    public String getValue() {
        return this.value;
    }

}
