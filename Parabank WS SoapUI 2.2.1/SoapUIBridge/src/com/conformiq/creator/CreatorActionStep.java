/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator;

import com.conformiq.designer.TestStep;

/**
 * A test step that contains a Creator action.
 * <p>
 * {@link #getType() getType()}
 * returns {@link com.conformiq.designer.TestStep.StepType#CREATOR_ACTION
 * CREATOR_ACTION} for objects implementing this interface.
 * 
 * @since API version 5
 */
public interface CreatorActionStep extends TestStep {
	/**
	 * Get the Creator action for this test step.
	 * 
	 * @return action for this step
	 */
	CreatorAction getAction();
}
