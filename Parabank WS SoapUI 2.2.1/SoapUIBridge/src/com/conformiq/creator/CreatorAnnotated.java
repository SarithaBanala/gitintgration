/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator;

import java.util.Map;

/**
 * Base interface for objects that can be annotated with Creator annotations.
 * 
 * @since API version 6
 */
public interface CreatorAnnotated {
	/**
	 * Get annotations for this object.
	 * 
	 * @return annotations for this object
	 */
	public Map<String, String> getAnnotations();
}
