/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator;

/**
 * Wrapper for all Creator values.
 * <p>
 * This interface is used to wrap values to support "don't care" and other
 * special value-like returns that are not concrete values.
 * 
 * @since API version 7
 */
public interface CreatorValueWrapper<T> {
	/**
	 * Check if this wrapper represents a "don't care" value.
	 * 
	 * @return {@code true} if this is a "don't care" value, else {@code false}
	 */
	public boolean isDontCare();
	
	/**
	 * Get the concrete value contained inside this wrapper.
	 * 
	 * @exception IllegalStateException if the value contained is not a concrete value 
	 */
	public T get();
}
