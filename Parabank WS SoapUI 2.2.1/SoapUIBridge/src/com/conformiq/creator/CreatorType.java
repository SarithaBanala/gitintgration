/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator;

/**
 * Base interface for all Creator type-like objects.
 * <p>
 * Type-like objects are objects that define the structure of the model and
 * generally have a representation on the structure diagram canvas.
 * 
 * @since API version 5
 */
public interface CreatorType {
	/**
	 * Get the domain of this type-like object.
	 * 
	 * @return type of this object
	 */
	public CreatorDomain getDomain();
	
	/**
	 * Get the name of this type-like object.
	 * <p>
	 * This returns the user-visible name and is not guaranteed to be unique.
	 * 
	 * @return name of this object
	 */
	public String getName();
	
	/**
	 * Get the identifier of this type-like object.
	 * <p>
	 * Identifiers are guaranteed to be globally unique within a model. The
	 * contents of the identifier should be considered opaque and should never
	 * be parsed in any way. They are also not usually meant to be displayed to
	 * end users.
	 * 
	 * @return identifier of this object
	 */
	public String getIdentifier();
}
