/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui;

import com.conformiq.creator.CreatorAnnotated;
import com.conformiq.creator.CreatorType;

/**
 * A widget.
 * <p>
 * Some types of widgets implement a subinterface with more information about the widget.
 * 
 * @see CreatorTextBox
 * @see CreatorDropdown
 * @see CreatorContainerWidget
 * 
 * @since API version 7
 */
public interface CreatorWidget extends CreatorType, CreatorAnnotated {
	/**
	 * Enumeration of all existing widget types.
	 */
	public enum Type {
		/**
		 * A screen widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget}.
		 */
		SCREEN,
		
		/**
		 * A popup widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget}.
		 */
		POPUP,
		
		/**
		 * A form widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget}.
		 */
		FORM,
		
		/**
		 * A group widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget}.
		 */
		GROUP,

		/**
		 * A tab widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget}.
		 */
		TAB,

		/**
		 * A text box widget.
		 * <p>
		 * Implements {@link CreatorTextBox}.
		 */
		TEXTBOX,

		/**
		 * A check box widget.
		 */
		CHECKBOX,

		/**
		 * A dropdown selection widget.
		 * <p>
		 * Implements {@link CreatorDropdown}.
		 */
		DROPDOWN,

		/**
		 * A radio button group widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget}.
		 */
		RADIOBUTTONGROUP,

		/**
		 * A radio button widget.
		 */
		RADIOBUTTON,

		/**
		 * A calendar widget.
		 */
		CALENDAR,

		/**
		 * A list box widget.
		 */
		LISTBOX,

		/**
		 * A button widget.
		 */
		BUTTON,

		/**
		 * A hyperlink widget.
		 */
		HYPERLINK,

		/**
		 * A table widget.
		 */
		TABLE,

		/**
		 * A tree node widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget} which may only contain other {@link #TREENODE} widgets.
		 */
		TREENODE,

		/**
		 * A menu bar widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget} which may only contain {@link #MENU} widgets.
		 */
		MENUBAR,

		/**
		 * A menu widget.
		 * <p>
		 * Implements {@link CreatorContainerWidget} which may only contain {@link #MENU}, {@link #CLICKCHOICE} and {@link #MOUSEOVERCHOICE} widgets.
		 */
		MENU,

		/**
		 * A menu entry widget that can be clicked.
		 */
		CLICKCHOICE,

		/**
		 * A menu entry widget that can be mouse overed.
		 */
		MOUSEOVERCHOICE,
		
		/**
		 * A label widget.
		 * 
		 * @since API version 10
		 */
		LABEL
	}

	/**
	 * Get the type of this widget.
	 * 
	 * @return type of this widget
	 */
	public Type getType();
}
