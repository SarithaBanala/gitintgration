/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui.value;

import java.util.List;

/**
 * Represents the value of a table widget.
 *
 * @since API version 7
 */
public interface CreatorTableValue extends CreatorWidgetValue {
	
	/**
	 * The values of widgets in a row of the table.
	 */
	public interface Row {
		
		/**
		 * Get the values of widgets in this row.
		 * 
		 * @return values of widgets
		 */
		public List<CreatorWidgetValue> getValues(); 
	}
	
	/**
	 * Get the values of rows in this table widget.
	 * 
	 * @return rows in table
	 */
	public List<Row> getRows();
}
