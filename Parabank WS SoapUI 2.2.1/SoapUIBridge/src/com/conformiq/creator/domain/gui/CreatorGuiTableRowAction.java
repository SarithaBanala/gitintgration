/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui;

import com.conformiq.creator.domain.gui.value.CreatorTableValue;

/**
 * Extended interface for representing actions targeted at table rows.
 * 
 * @since API version 7
 */
public interface CreatorGuiTableRowAction extends CreatorGuiAction {
	
	/**
	 * Get the row this action is targeted at.
	 * 
	 * @return target row
	 */
	public CreatorTableValue.Row getTargetRow();
}
