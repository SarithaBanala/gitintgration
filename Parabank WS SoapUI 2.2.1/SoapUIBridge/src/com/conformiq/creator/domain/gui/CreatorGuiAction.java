/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui;

import java.util.List;

import com.conformiq.creator.CreatorAction;
import com.conformiq.creator.domain.gui.value.CreatorWidgetValue;

/**
 * An action in the {@link com.conformiq.creator.CreatorDomain#GUI} domain.
 * 
 * @since API version 7
 */
public interface CreatorGuiAction extends CreatorAction {
	public enum Kind {
		DISPLAY,
		FILL,
		MOUSEOVER,
		SELECT,
		CLICK
	}
	
	/**
	 * Get the kind of this action.
	 * 
	 * @return kind of this action
	 */
	public Kind getActionKind();
	
	/**
	 * Get the context of this action.
	 * <p>
	 * The returned list contains the containing widgets of the target widget of
	 * this action, starting from the direct parent and ending in a top level
	 * widget.
	 * 
	 * @return context of this action
	 */
	public List<CreatorWidget> getContext();
	
	/**
	 * Get the value of the target widget for this action.
	 * 
	 * @return value of the target widget
	 */
	public CreatorWidgetValue getValue();
}
