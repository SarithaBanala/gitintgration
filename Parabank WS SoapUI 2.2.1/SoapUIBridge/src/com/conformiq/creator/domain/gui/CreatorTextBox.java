/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui;

/**
 * A text box widget.
 * <p>
 * {@link #getType()} always returns {@link CreatorWidget.Type#TEXTBOX} for objects that
 * implement this interface.
 * 
 * @since API version 7
 */
public interface CreatorTextBox extends CreatorWidget {
	/**
	 * The kind of a text box widget.
	 */
	public enum Kind {
		/**
		 * A string text box, may contain any text.
		 */
		STRING,
		
		/**
		 * A number text box, may only contain numbers.
		 */
		NUMBER
	}
	
	/**
	 * Get the kind of this text box widget.
	 * 
	 * @return kind of this text box
	 */
	Kind getKind();
}
