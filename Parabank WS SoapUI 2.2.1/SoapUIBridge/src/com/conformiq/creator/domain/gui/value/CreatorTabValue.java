/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui.value;

/**
 * Represents the value of a tab widget that can contain other widgets.
 * 
 * @since API version 7
 */
public interface CreatorTabValue extends CreatorContainerWidgetValue {
	
	/**
	 * Check if this tab is selected.
	 * 
	 * @return {@code true} if this tab is selected, else {@code false}
	 */
	public boolean isSelected();
}
