/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui;

import java.util.List;

/**
 * A dropdown widget.
 * <p>
 * {@link #getType()} always returns {@link CreatorWidget.Type#DROPDOWN} for objects that
 * implement this interface.
 *
 * @since API version 7
 */
public interface CreatorDropdown extends CreatorWidget {
	/**
	 * Get the choices for this dropdown.
	 * 
	 * @return choices for this dropdown
	 */
	public List<String> getChoices();
}
