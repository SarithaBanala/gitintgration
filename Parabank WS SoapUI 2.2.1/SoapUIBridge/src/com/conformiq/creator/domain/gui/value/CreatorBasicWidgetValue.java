/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui.value;

import com.conformiq.creator.CreatorValueWrapper;

/**
 * Represents the value of a basic widget that does not contain other widgets.
 * <p>
 * Some widget values implement extended interfaces providing additional information.
 * @see CreatorCalendarValue
 * @see CreatorCheckBoxValue
 * @see CreatorDropdownValue
 * @see CreatorListBoxValue
 * @see CreatorTextBoxValue
 * @see CreatorLabelValue
 * 
 * @since API version 7
 */
public interface CreatorBasicWidgetValue extends CreatorWidgetValue {
	
	/**
	 * Get the widget status of this widget.
	 * 
	 * @return widget status
	 */
	CreatorValueWrapper<CreatorWidgetStatus> getStatus();
}
