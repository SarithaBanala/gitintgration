/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui;

import java.util.List;

/**
 * Base interface for widgets that contain other widgets.
 *
 * @since API version 7
 */
public interface CreatorContainerWidget extends CreatorWidget {
	/**
	 * Get the widgets contained.
	 * 
	 * @return contained widgets
	 */
	public List<CreatorWidget> getChildren();
}
