/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui.value;

import com.conformiq.creator.CreatorValueWrapper;

/**
 * Represents the value of a label widget.
 * 
 * @since API version 10
 */
public interface CreatorLabelValue extends CreatorBasicWidgetValue {
    
    /**
     * Get the text contained in this label widget.
     * 
     * @return text in this label
     */
    public CreatorValueWrapper<String> getText();
}
