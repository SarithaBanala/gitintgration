/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui.value;

/**
 * Enumerates the possible values of widget status.
 * 
 * @since API version 7
 */
public enum CreatorWidgetStatus {
	ENABLED,
	DISABLED,
	HIDDEN,
	VISIBLE
}