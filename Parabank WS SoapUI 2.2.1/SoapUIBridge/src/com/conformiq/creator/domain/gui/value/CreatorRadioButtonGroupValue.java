/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.gui.value;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.gui.CreatorWidget;

/**
 * Represents a radio button group widget.
 * <p>
 * A radio button group is a special container widget that can only contain
 * button widgets, of which one can be selected.
 * 
 * @since API version 7
 */
public interface CreatorRadioButtonGroupValue extends CreatorContainerWidgetValue {
	
	/**
	 * Get the selected button for this radio button group.
	 * 
	 * @return selected button
	 */
	public CreatorValueWrapper<CreatorWidget> getSelected();
}
