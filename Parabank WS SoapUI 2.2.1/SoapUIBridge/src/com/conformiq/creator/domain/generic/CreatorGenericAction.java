/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import com.conformiq.creator.CreatorAction;

/**
 * Base interface for actions in the
 * {@link com.conformiq.creator.CreatorDomain#GENERIC} domain.
 * 
 * @since API version 5
 */
public interface CreatorGenericAction extends CreatorAction {
	
	/**
	 * Enumeration of all action types in the
	 * {@link com.conformiq.creator.CreatorDomain#GENERIC} domain.
	 */
	public enum Kind {
		MESSAGE,
		CUSTOM
	}
	
	/**
	 * Get the type of this action.
	 * 
	 * @return type of this action
	 */
	public Kind getActionKind();
}
