/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import java.util.List;

/**
 * Type information for a message.
 * 
 * @since API version 5
 */
public interface CreatorMessageType extends CreatorStructuredType {
	/**
	 * Get the interfaces associated with this message.
	 * <p>
	 * The returned list contains all interfaces this message type can be sent
	 * or received through.
	 * 
	 * @return associated interfaces
	 */
	List<CreatorExternalInterface> getInterfaces();
}
