/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import com.conformiq.creator.domain.generic.value.CreatorCustomActionValue;

/**
 * Interface for representing a Creator custom action.
 * <p>
 * {@link #getActionKind()} always returns
 * {@link CreatorGenericAction.Kind#CUSTOM} for objects implementing this
 * interface.
 * 
 * @since API version 5
 */
public interface CreatorCustomAction extends CreatorGenericAction {
	/**
	 * Get the external interface this action is associated with.
	 *
	 * @return interface of this action
	 */
	public CreatorExternalInterface getInterface();
	
	/**
	 * Get the value for this custom action.
	 * 
	 * @return value for this custom action
	 */
	public CreatorCustomActionValue getCustomAction();
}
