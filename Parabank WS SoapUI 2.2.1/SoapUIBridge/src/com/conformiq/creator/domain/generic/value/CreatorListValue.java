/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic.value;

import java.util.List;

import com.conformiq.creator.CreatorValueWrapper;

/**
 * A value containing a list of values.
 * 
 * @since API version 5
 */
public interface CreatorListValue extends CreatorValue {
	/**
	 * Get the contents of the list.
	 * 
	 * @return contents of the list
	 */
	public List<CreatorValueWrapper<CreatorValue>> getContents(); 
}
