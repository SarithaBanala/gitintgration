/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import com.conformiq.creator.CreatorAnnotated;

/**
 * An external interface of the SUT.
 * 
 * @since API version 5
 */
public interface CreatorExternalInterface extends CreatorAnnotated {
	public enum Direction {
		INPUT,
		OUTPUT
	}
	
	/**
	 * Returns the name of this interface as defined in the structure diagram.
	 * 
	 * @return name of the interface
	 */
	public String getName();
	
	/**
	 * Returns the direction of this interface as seen from the perspective of the SUT.
	 * 
	 * @return direction of the interface
	 */
	public Direction getDirection();
}
