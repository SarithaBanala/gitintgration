/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

/**
 * Interface representing a list of elements of another type.
 * 
 * @since API version 5
 */
public interface CreatorListType extends CreatorGenericType {
	/**
	 * Get the type of the list elements.
	 * 
	 * @return type of list elements
	 */
	public CreatorGenericType getElementType();
}
