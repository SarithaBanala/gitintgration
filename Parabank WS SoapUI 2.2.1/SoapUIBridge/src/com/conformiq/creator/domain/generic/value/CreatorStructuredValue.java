/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic.value;

import java.util.List;

import com.conformiq.creator.domain.generic.CreatorTypeField;

/**
 * Base interface for structured values.
 * 
 * @since API version 5
 */
public interface CreatorStructuredValue extends CreatorValue {
	/**
	 * Get the values of fields contained in this structured value.
	 * 
	 * @return values of contained fields
	 */
	public List<CreatorValueField> getFields();
	
	/**
	 * Get the value of a specific field based on {@link CreatorTypeField}.
	 * 
	 * @param field field to get the value of
	 * @return value of the field
	 */
	public CreatorValueField getFieldValue(CreatorTypeField field);
}
