/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import java.util.List;

/**
 * Type information for a custom action.
 * 
 * @since API version 5
 */
public interface CreatorCustomActionType extends CreatorMessageType {
	
	/**
	 * An element of the textual rendering for a custom action.
	 */
	public interface TextualRenderingToken {
		
		/**
		 * Types of textual rendering tokens.
		 */
		public enum Token {
			LITERAL,
			FIELDREF
		}
		
		/**
		 * Get the type of this token.
		 * 
		 * @return type of token
		 */
		public Token getToken();
	}
	
	/**
	 * A literal string token.
	 * <p>
	 * {@link #getToken()} returns {@link Token#LITERAL} for objects
	 * implementing this interface.
	 */
	public interface LiteralToken extends TextualRenderingToken {
		/**
		 * Get the string to be rendered as text for this token.
		 * 
		 * @return string to render
		 */
		public String getText();
	}
	
	/**
	 * A field reference token.
	 * <p>
	 * {@link #getToken()} returns {@link Token#FIELDREF} for objects
	 * implementing this interface.
	 */
	public interface FieldRefToken extends TextualRenderingToken {
		/**
		 * Get the field whose value is to be rendered as text for this token.
		 * 
		 * @return field to render
		 */
		public CreatorTypeField getField();
	}

	/**
	 * Get the user-visible textual rendering for this custom action.
	 * <p>
	 * Each token should be rendered in order, with no extra characters between,
	 * according to the rules for the type of each token.
	 * 
	 * @return list of tokens to render
	 */
	public List<TextualRenderingToken> getTextualRendering();
}
