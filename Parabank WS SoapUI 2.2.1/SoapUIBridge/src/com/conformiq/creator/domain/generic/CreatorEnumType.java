/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import java.util.List;

/**
 * Interface for representing enumeration types.
 * 
 * @since API version 5
 */
public interface CreatorEnumType extends CreatorGenericType {
	
	/**
	 * Get the possible values for this enumeration type.
	 * 
	 * @return possible values for this enumeration
	 */
	List<String> getValues();
}
