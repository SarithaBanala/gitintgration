/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic.value;

/**
 * A value containing a choice type value.
 * 
 * @since API version 5
 */
public interface CreatorChoiceValue extends CreatorStructuredValue {
	/**
	 * Get the selected field of this choice value.
	 * 
	 * @return selected field
	 */
	public CreatorValueField getSelectedField();
}
