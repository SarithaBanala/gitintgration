/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import com.conformiq.creator.domain.generic.value.CreatorMessageValue;

/**
 * Interface for representing a message send or receive action.
 * <p>
 * {@link #getActionKind()} always returns
 * {@link CreatorGenericAction.Kind#MESSAGE} for objects implementing this
 * interface.
 * 
 * @since API version 5
 */
public interface CreatorMessageAction extends CreatorGenericAction {
	
	/**
	 * Get the interface through which the message is sent or received.
	 * 
	 * @return interface for this action
	 */
	public CreatorExternalInterface getInterface();
	
	/**
	 * Get the value of the message sent or received in this action.
	 * 
	 * @return message value for this action
	 */
	public CreatorMessageValue getMessage();
}
