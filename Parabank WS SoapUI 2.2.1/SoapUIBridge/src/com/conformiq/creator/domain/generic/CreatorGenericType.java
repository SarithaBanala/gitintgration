/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import com.conformiq.creator.CreatorType;

/**
 * Base interface for types in the {@link com.conformiq.creator.CreatorDomain#GENERIC} domain.
 * 
 * @since API version 5
 */
public interface CreatorGenericType extends CreatorType {
	
	/**
	 * Enumeration of types in the {@link com.conformiq.creator.CreatorDomain#GENERIC} domain.
	 */
	public enum Type {
		// Basic types
		NUMBER,
		STRING,
		BOOLEAN,
		DATE,

		// Special
		ENUMERATION,
		LIST,
		
		// Structured types
		MESSAGE,
		CUSTOMACTION,
		SEQUENCE,
		CHOICE
	}
	
	/**
	 * Get the type of this type object.
	 * <p>
	 * This can be used instead of testing with {@code instanceof} to determine
	 * the interfaces implemented by this type object.
	 * 
	 * For example, both of these are equally valid:
	 * <pre>
	 * {@code
	 * if (type instanceof CreatorEnumType)
	 *   doSomethingWithEnumType((CreatorEnumType)type);
	 * }
	 * </pre>
	 * <pre>
	 * {@code
	 * if (type.getType() == CreatorType.Type.ENUMERATION)
	 *   doSomethingWithEnumType((CreatorEnumType)type);
	 * }
	 * </pre>
	 * 
	 * @return type of this type object
	 */
	public Type getType();
	
	/**
	 * Get the namespace of this type object.
	 * 
	 * @since API version 6
	 */
	public String getNamespace();
}
