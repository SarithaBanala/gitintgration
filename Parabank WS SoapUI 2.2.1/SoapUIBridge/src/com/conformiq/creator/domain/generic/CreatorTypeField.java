/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

/**
 * Extended type information for a field of a structured type.
 * 
 * @since API version 5
 */
public interface CreatorTypeField {
	/**
	 * Get the name of this field.
	 * 
	 * @return name of field
	 */
	public String getName();
	
	/**
	 * Get the namespace of this field.
	 * <p>
	 * This is mainly useful when a model is based on imported XSD schema and
	 * the scripter needs to produce output matching the original schema.
	 * 
	 * @return namespace of the field
	 * @since API version 6
	 */
	public String getNamespace();
	
	/**
	 * Get the type of this field.
	 * 
	 * @return type of field
	 */
	public CreatorGenericType getType();
	
	/**
	 * Check if this field is optional.
	 * 
	 * @return {@code true} if this field is optional, otherwise {@code false}
	 */
	public boolean isOptional();
}
