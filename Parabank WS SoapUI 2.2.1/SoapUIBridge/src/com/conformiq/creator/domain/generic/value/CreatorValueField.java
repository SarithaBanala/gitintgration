/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic.value;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.generic.CreatorTypeField;

/**
 * The value of a field in a structured value.
 * 
 * @since API version 5
 */
public interface CreatorValueField {
	/**
	 * Get the user-visible name of this field.
	 * 
	 * @return name of this field
	 */
	public String getName();
	
	/**
	 * Get the value contained in this field value.
	 * 
	 * @return value contained
	 */
	public CreatorValueWrapper<CreatorValue> getValue();
	
	/**
	 * Get the field type of this field value.
	 * 
	 * @since API version 6
	 * @return field type of this field value
	 */
	public CreatorTypeField getType();
}
