/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import java.util.List;

/**
 * Interface representing a structured type.
 * 
 * @since API version 5
 */
public interface CreatorStructuredType extends CreatorGenericType {
	
	/**
	 * Get field information for fields contained in this structured type.
	 * 
	 * @return fields for this structured type
	 */
	public List<CreatorTypeField> getFields();
	
	/**
	 * Get field information by unique identifier of field.
	 * 
	 * @param identifier
	 *            identifier of field to fetch
	 * @return field information
	 */
	public CreatorTypeField getFieldByIdentifier(String identifier);
}
