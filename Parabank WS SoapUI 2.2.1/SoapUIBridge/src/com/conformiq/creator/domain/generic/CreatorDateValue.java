/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic;

import java.util.Date;

import com.conformiq.creator.domain.generic.value.CreatorValue;

/**
 * A value containing a date.
 * 
 * @since API version 5
 */
public interface CreatorDateValue extends CreatorValue {
	
	/**
	 * Get the date contained in this value.
	 * 
	 * @return contained date
	 */
	public Date getDate();
}
