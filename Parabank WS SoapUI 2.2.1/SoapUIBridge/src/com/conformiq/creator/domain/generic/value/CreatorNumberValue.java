/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator.domain.generic.value;

import java.math.BigInteger;

/**
 * A value containing a number.
 * 
 * @since API version 5
 */
public interface CreatorNumberValue extends CreatorValue {
	/**
	 * Get the number contained in this value.
	 * 
	 * @return number contained in this value
	 */
	public BigInteger getNumber();
}
