/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator;

/**
 * The base interface for Creator actions contained in test steps.
 * 
 * @since API version 5
 */
public interface CreatorAction {
	public enum ActionType {
		INPUT,
		VERIFICATION
	}
	
	/**
	 * Get the Creator domain of this action.
	 * 
	 * @return domain of this action
	 */
	public CreatorDomain getActionDomain();
	
	/**
	 * Get the type of this action.
	 *  
	 * @return type of this action
	 */
	public ActionType getActionType();
}
