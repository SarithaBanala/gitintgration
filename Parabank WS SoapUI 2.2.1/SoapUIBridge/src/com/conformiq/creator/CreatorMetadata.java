/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.creator;

import java.util.Collection;

import com.conformiq.creator.domain.generic.CreatorExternalInterface;
import com.conformiq.designer.Metadata;
import com.conformiq.designer.ModelingNotation;

/**
 * Interface for accessing Creator diagram modeling specific metadata.
 * <p>
 * {@link com.conformiq.designer.Project#getMetadata(ModelingNotation)} will return an object that
 * implements this interface when queried with {@link com.conformiq.designer.ModelingNotation#CREATOR}.
 * 
 * @since API version 7
 */
public interface CreatorMetadata extends Metadata {
	/**
	 * Get all top level (ie. not contained inside another type) Creator types defined in the model.
	 * 
	 * @return collection containing top level types
	 */
	public Collection<CreatorType> getTypes();
	
	/**
	 * Get all external interfaces defined in the model.
	 * 
	 * @return collection containing external interfaces
	 */
	public Collection<CreatorExternalInterface> getExternalInterfaces();
}
