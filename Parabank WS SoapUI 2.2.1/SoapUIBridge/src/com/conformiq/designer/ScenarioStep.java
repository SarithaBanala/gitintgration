/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

/**
 * @since API version 11
 */
public interface ScenarioStep extends ExecutionInfoStep {

	/**
	 * Get scenario text as specified in the model.
	 * @return the scenario as specified in the model
	 */
	public String getScenario();
}
