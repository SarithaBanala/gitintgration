/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import com.conformiq.qtronic2.QMLRecord;

public interface InternalMessageStep extends TestStep {
	
	/**
	 * Returns a name of sending thread.
	 *
	 * @return a sender thread
	 */
	String getSender();

	/**
	 * Returns a name of receiving thread.
	 *
	 * @return a receiver thread
	 */
	String getReceiver();
	
	/**
	 * Returns the port to which the message was sent or from which it was received.
	 *
	 * @return a port of the message
	 */
	PortDefinition getPort();
	
	/**
	 * Returns a message that was sent in this step.
	 *
	 * @return a message that was sent in this step
	 */
	QMLRecord getMessage();
}
