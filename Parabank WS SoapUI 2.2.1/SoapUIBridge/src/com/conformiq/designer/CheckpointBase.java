/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.Map;

import com.conformiq.designer.DesignConfiguration.CoverageSetting;

public interface CheckpointBase {

	/**
	 * Return a short name of this checkpoint
	 * 
	 * @deprecated This was originally documented as returning the full name of
	 *             the checkpoint, but in reality it returns short name for
	 *             checkpoints and full name for checkpoint groups. To avoid
	 *             confusion, please use {@link #getFullName()} or
	 *             {@link #getShortName()} explicitly.
	 */
	@Deprecated
	String getName();

	/**
	 * Return the full name of this checkpoint
	 * 
	 * The full name of a checkpoint contains slash '/' separated components of
	 * the checkpoint name as one long string.
	 * 
	 * @since API version 6
	 */
	String getFullName();
	
	/**
	 * Return a short name of this checkpoint
	 * 
	 * The short name of a checkpoint contains only
	 * last visible part of the checkpoint, for example
	 * state's name or last part of a requirements name.
	 */
	String getShortName();

	/**
	 * Return unique internal name of this checkpoint.
	 * 
	 * Returned unique internal name of the checkpoint is unique within
	 * the model. No other checkpoint has same identifier.
	 *
     * The format of the string returned by this method has been changed in API version 11.
	 */
	String getIdentifier();

	/**
	 * Returns a parent checkpoint in the checkpoint grouping.
	 * 
	 * If this is a top level checkpoint, this method returns <code>null</code>.
	 * 
	 * @return	a parent checkpoint, or <code>null</code>
	 */
	CheckpointGroup getParent();
	
	/**
	 * Returns coverage setting of the checkpoint in given design configuration.
	 * 
	 * This returns always a coverage setting of the checkpoint at the time of last
	 * test generation.
	 * 
	 * @param dc	a design configuration whose coverage settings will be returned
	 * @return		a coverage setting for the checkpoint in the design configuration
	 */
	CoverageSetting getCoverageSetting(DesignConfiguration dc);

	/**
	 * Returns the custom attributes associated with the checkpoint.
	 * 
	 * @return     an unmodifiable map of the custom attributes
	 */
	Map<String, String> getCustomAttributes();
}
