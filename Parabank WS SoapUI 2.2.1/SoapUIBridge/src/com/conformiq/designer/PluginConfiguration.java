/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.Iterator;
import java.util.Map;

import com.conformiq.qtronic2.HostConfigurator;

public interface PluginConfiguration extends Iterable<Map.Entry<String, String>> {
	/**
	 * Return value of given configuration property, or <code>null</code>
	 * if configuration does not contain such property.
	 * 
	 * @param property	a property whose value is returned
	 * @return			a value in current configuration
	 */
	String getConfigurationOption(String property);
	
	/**
	 * Return iterator for all configuration properties and their values.
	 * 
	 * @return	iterator to mappings of configuration settings
	 */
	Iterator<Map.Entry<String, String>> iterator();
	
	/**
	 * Return object through which script host options can be configured.
	 * 
	 * @return  host configurator instance
	 * 
	 * @since API version 3
	 */
	HostConfigurator getHostConfigurator();
}
