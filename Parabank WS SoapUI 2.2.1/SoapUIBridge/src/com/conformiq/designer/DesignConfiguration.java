/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.List;

public interface DesignConfiguration extends Iterable<TestCase> {

	enum CoverageSetting { DONT_CARE, COVER, BLOCK, ASSERT_ERROR }

	/**
	 * Returns a name of the design configuration.
	 *
	 * @return a name of the design configuration
	 */
	String getName();
	
	/**
	 * Returns a stable identifier for the design configuration. This identifier
     * is guaranteed not to change over renames and other modifications. It is
     * to be considered opaque, no attempt to parse the contents should be done.
     * 
     * @return the stable opaque identifier of the design configuration
     * @since API version 11
     */
	String getIdentifier();
	
	/**
	 * Returns a list of test cases that belong to this design configuration.
	 *
	 * @return a list of design configuration's test cases 
	 */
	List<TestCase> getTestCases();
	
	/**
	 * Returns a coverage setting for the checkpoint in the design configuration that was used
	 * to generate tests.
	 * 
	 * @param checkpoint the checkpoint whose coverage setting value is to be returned
	 * @return a coverage setting value used for the checkpoint
	 */
	CoverageSetting getCoverageSetting(CheckpointBase checkpoint);
	
	/**
	 * Returns <tt>true</tt> if the coverage setting for given checkpoint is set to
	 * <tt>inherit</tt>, and used setting is resolved from parent checkpoint.
	 * 
	 * @param checkpoint the checkpoint whose coverage setting is checked
	 * @return <tt>true</tt> if the coverage setting is inherited from parent
	 */
	boolean isCoverageSettingInherited(CheckpointBase checkpoint);
	
	/**
	 * Return a list of checkpoints not covered in any test case in this design configuration.
	 * 
	 * @return	a list of checkpoints not covered by any test case
	 */
	List<Checkpoint> getUncoveredCheckpoints();
}
