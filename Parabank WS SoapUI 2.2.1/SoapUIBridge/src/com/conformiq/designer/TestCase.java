/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.List;

import com.conformiq.qtronic2.TimeStamp;

public interface TestCase extends Iterable<TestStep> {

	/**
	 * An exception thrown by methods which are valid only if
	 * tests were generated with OSI Methodology Support enabled. 
	 */
	class NoOSIMethodologySupportException extends RuntimeException {
		static final long serialVersionUID = 2805659276467332284L;
		
		public NoOSIMethodologySupportException(String message) {
			super(message);
		}
	}
	
	/**
	 * Returns a name of the test case.
	 *
	 * @return a name of the test case
	 */
	String getName();
	
	/**
	 * Returns an unique identifier of the test case.
	 *
	 * @return an identifier
	 */
	int getID();
	
	/**
	 * Returns a priority of the test case within the test suite.
	 *
	 * @return a priority
	 * 
	 * @deprecated Use {@link #getPriority()} instead.
	 */
	@Deprecated
	int getPriority(DesignConfiguration dc);
	
	/**
	 * Returns the priority of the test case.
	 * 
	 * @return the priority of the test case
	 * 
	 * @since API version 11
	 */
	int getPriority();
	
	/**
	 * Returns a probability of the test case.
	 *
	 * @return a probability
	 */
	double getProbability();
	
	/**
	 * Returns the checkpoint which is the main target for the test case. A test case has a main
	 * target only if the test suite was generated with OSI Methodology Support (ISO 9646-1 mode)
	 * enabled.
	 *
	 * @return a checkpoint, or throws an exception
	 * 
	 * @throws NoOSIMethodologySupportException if tests were generated without OSI methodology support mode
	 */
	Checkpoint getMainTarget() throws NoOSIMethodologySupportException;
	
	/**
	 * Returns the description of the test case.
	 * 
	 * @return a description string
	 */
	String getDescription();
	
	/**
	 * Returns a list of test steps of the test case.
	 *
	 * @return a list of test steps
	 */
	List<TestStep> getTestSteps();
	
	/**
	 * Returns the time stamp when the test case ends.
	 * 
	 * @return a time stamp of the end time of the test case
	 */
	TimeStamp getEndTime();

	/**
	 * Get list of test cases which are prerequisites for this test case;
	 * in other words, this test case would likely fail
     * during test execution if test case any of prerequisite test cases fail.  Note
     * that it is possible that there are both forward and backward
     * dependencies in the test suite (relative to the order in which test
     * cases are published) even though the goal is to have only backward
     * dependencies (later test cases depend on earlier ones).
	 * 
	 * @return	a list of prerequisite test cases
	 *
	 * @throws NoOSIMethodologySupportException if tests were generated without OSI methodology support mode
	 */
	List<TestCase> getPrerequisites() throws NoOSIMethodologySupportException;
	
	/**
	 * Get list of test cases which are dependents for this test case;
	 * in other words, a dependent test case would likely fail
     * during test execution if this test case fails.  Note
     * that it is possible that there are both forward and backward
     * dependencies in the test suite (relative to the order in which test
     * cases are published) even though the goal is to have only backward
     * dependencies (later test cases depend on earlier ones).
	 * 
	 * @return	a list of dependent test cases
	 *
	 * @throws NoOSIMethodologySupportException if tests were generated without OSI methodology support mode
	 */
	List<TestCase> getDependents() throws NoOSIMethodologySupportException;

	/**
	 * Get precondition test case for the given test. The precondition test, if
	 * not NULL, needs to be executed before the given test case. Routine
	 * returns NULL when there is no precondition test
	 * 
	 * @since API version 2
	 */
	public TestCase getPreconditionTest();
}
