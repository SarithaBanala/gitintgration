/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

/**
 * Interface for script back-ends. This interface is an alternative to a event
 * based script back-end API implemented in an abstract class
 * {@link com.conformiq.qtronic2.ScriptBackend}. Classes implementing this
 * interface can query the project and the test generation data. Conformiq
 * Designer calls the method {@code renderTests} of the implementing class, and
 * the implementing class can read required information of the project and the
 * test generation data by querying the data in any order.
 * 
 * Implementing classes may notify about rendering progress by calling method
 * {@code progress} in {@link ProgressNotificationSink}.
 * 
 * The method must return <code>true</code> if rendering with the script
 * back-end succeeded, and <code>false</code> otherwise.
 */
public interface QueryScriptBackend {
	/**
	 * Prepare the scripter for rendering.
	 * 
	 * @param conf plugin configuration interface
	 */
	void preRender(PluginConfiguration conf);
	
	/**
	 * Render the tests in the given design configuration.
	 * 
	 * @param configuration	a configuration of the script back-end plugin
	 * @param project		a project information in use
	 * @param dc			a design configuration this script back-end is attached to
	 * @param ns			a notification sink for notification messages and progress
	 * @return				rendering status, <code>true</code> if rendering succeeded
	 */
	boolean renderTests(
			PluginConfiguration configuration,
			Project project,
			DesignConfiguration dc,
			ProgressNotificationSink ns);
}
