/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

public interface ExecutionInfoStep extends TestStep {

    /**
     * Returns a string representing the information.
     *
     * @return an info string
     *
     * @deprecated since API version 11, cast {@link #ExecutionInfoStep} to more
     *             refined types like f.ex. {@link #ScenarioStep} to access
     *             data.
     */
    @Deprecated
    String getInformation();
}
