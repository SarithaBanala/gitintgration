/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import com.conformiq.qtronic2.NotificationSink;

/**
 * Interface for modeling frontend plugin callbacks.
 * An object that implements the {@link ModelingFrontendPlugin} interface communicates
 * with an object that implements the ModelingFrontendCallback interface.
 */
public interface ModelingFrontendCallback extends NotificationSink
{
    /**
     * Export the generated Conformiq model part, and let Designer
     * generate filename for it.
     * <p>
     * Throws exceptions for invalid parameter values.
     * Always closes InputStream content.
     * 
     * @param content  Content of this stream represents the exported
     *                 model part.
     */
    public void exportModelPart(java.io.Reader content);

    /**
     * Export the generated Conformiq model part with given filename.
     * <p> 
     * Conformiq Designer will save generated model part in to model
     * subdirectory specified in Modeling Frontend's configuration,
     * "generated" by default.
     * <p>
     * Throws exceptions for invalid parameter values.
     * Always closes InputStream content.
     * 
     * @param content  Content of this stream represents the exported
     *                 model part.
     * @param filename A filename for the exported model part.
     */
    public void exportModelPart(java.io.Reader content, String filename);

    public static final int SEVERITY_INFO = 1;
    public static final int SEVERITY_WARNING = 2;
    public static final int SEVERITY_ERROR = 3;

    /**
     * Add markers to the current data input file.
     * <p>
     * Throws exceptions for invalid parameter values.
     *
     * @param severity one of the above severity constants.
     * @param line line number of problem in current input file.
     * @param message text content of the problem.
     */
    public void notify(int severity, int line, java.lang.String message);
}
