/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.List;

import com.conformiq.qtronic2.Annotated;
import com.conformiq.qtronic2.QMLRecordType;

public interface PortDefinition extends Annotated {
	/**
	 * Type of a port.
	 */
	enum PortType {
		INBOUND,
		OUTBOUND,
		INTERNAL
	}

	/**
	 * Return name of the port.
	 * 
	 * @return name of this port
	 */
	String getName();
	
	/**
	 * Return the QML identifier of the port.
	 * 
	 * @since API version 5
	 */
	String getIdentifier();
	
	/**
	 * Return type of the port.
	 * 
	 * @return type of this port
	 */
	PortType getType();

	/**
	 * Return a list of valid message types of this port.
	 * 
	 * Internal ports can be used to send any records, and
	 * for internal ports this method returns an empty list.
	 * 
	 * @return	a list of message types that can be sent via this port
	 */
	List<QMLRecordType> getMessages();
}
