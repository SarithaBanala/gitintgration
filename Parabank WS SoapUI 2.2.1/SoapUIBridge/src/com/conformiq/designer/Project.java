/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.Collection;
import java.util.List;

import com.conformiq.qtronic2.MetaDataDictionary;

@SuppressWarnings("deprecation")
public interface Project {
    /**
     * Get meta-data dictionary.
     * 
     * Meta-data dictionary contains information about the model in the project.
     * 
     * @deprecated Use {@link #getMetadata(ModelingNotation)} instead.
     */
	@Deprecated
    MetaDataDictionary getMetaData();
	
	/**
	 * Get metadata for the part of the model that uses specified modeling
	 * notation.
	 * <p>
	 * The return value should be cast to the interface type corresponding with
	 * the specified {@link ModelingNotation}.
	 *
	 * @since API version 7
	 * @param notation modeling notation to fetch metadata for
	 * @return metadata for the project, or {@code null} if there is no metadata
	 */
	Metadata getMetadata(ModelingNotation notation);
    
    /**
     * Return a list of all top level checkpoint groups.
     * 
     * @return list of top level checkpoint groups
     */
    List<CheckpointGroup> getTopLevelCheckpoints();
    
    /**
     * Resolve a checkpoint by its identifier.
     * 
     * @param identifier	an identifier for the wanted checkpoint
     * @return				a checkpoint, or <code>null</code> if there is no such checkpoint
     */
    CheckpointBase getCheckpoint(String identifier);
    
    /**
     * Return a list of inbound and outbound ports, ie. the system block
     * of the projects' model.
     * 
     * @return	a list of inbound and outbound ports
     * 
     * @deprecated Use {@link QMLMetadata#getPorts()} instead.
     */
    @Deprecated
    List<PortDefinition> getPorts();
    
    /**
     * Return information about external requirement providers associated with the project.
     * 
     * @return collection of objects that provide information about external requirement providers
     * 
     * @since API version 8
     */
    Collection<RequirementProviderInfo> getRequirementProviders();
}
