/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.Map;

/** 
 * Information about an external requirement provider.
 * 
 * @since API version 8
 */
public interface RequirementProviderInfo {
    /**
     * Get the identifier of the provider. This uniquely identifies the type of the provider.
     * 
     * @return identifier of the provider type
     */
    String getIdentifier();
    
    /**
     * Get the name of the provider instance. This is a string intended to be
     * shown to the user to identify the specific requirement provider instance.
     * 
     * @return name of the provider instance
     */
    String getName();
    
    /**
     * Get the configuration properties of the provider instance. This is
     * intended for scripters that need to upload information back to the
     * external requirement system the requirements originated from. The
     * contents are not formally defined and are up to the implementer of the
     * provider and scripter to agree on.
     * 
     * @return configuration properties of the provider instance
     */
    Map<String, String> getProperties();
}
