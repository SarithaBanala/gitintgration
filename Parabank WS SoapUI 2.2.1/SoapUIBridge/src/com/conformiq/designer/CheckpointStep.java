/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

public interface CheckpointStep extends TestStep {
	
	/**
	 * Returns a checkpoint covered in this test step.
	 *
	 * @return a checkpoint
	 */
	Checkpoint getCheckpoint();
	
	/**
	 * Returns a name of the execution thread of the step.
	 *
	 * @return a name of the thread
	 */
	String getThread();
}
