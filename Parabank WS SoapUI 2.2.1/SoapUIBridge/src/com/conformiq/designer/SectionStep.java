/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

public interface SectionStep extends TestStep {
	
	enum Section { PREAMBLE, BODY, POSTAMBLE, CUSTOM }
	
	/**
	 * Returns a section which starts at this point.
	 *
	 * @return a section starting from this marker
	 */
	Section getSection();

	/** Get name of custom section.
	 *
	 * @return Name of the custom section, or null if and only if getSection() != CUSTOM
	 */
	String getCustomSectionName();
}
