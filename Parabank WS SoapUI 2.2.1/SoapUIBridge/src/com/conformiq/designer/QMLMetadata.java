/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.Collection;

import com.conformiq.qtronic2.QMLRecordType;

/**
 * Interface for accessing QML language specific metadata.
 * <p>
 * {@link com.conformiq.designer.Project#getMetadata(ModelingNotation)} will return an object that
 * implements this interface when queried with {@link com.conformiq.designer.ModelingNotation#QML}.
 * 
 * @since API version 7
 */
public interface QMLMetadata extends Metadata {
    /**
     * Get all defined ports.
     */
    public Collection<PortDefinition> getPorts();
    
    /**
     * Find a specific port by name.
     */
    public PortDefinition findPort(String name);

    /**
     * Get all the top level QML record types defined in the model.
     * The result does not contain inner record types.
     */
    public Collection<QMLRecordType> getTypes();

    /**
     * Access type by its name.
     */
    public QMLRecordType getType(String name);
}
