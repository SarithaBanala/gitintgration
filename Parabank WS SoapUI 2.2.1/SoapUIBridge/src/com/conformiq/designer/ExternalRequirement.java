/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

/**
 * A requirement in an external requirement management system.
 * 
 * @since API version 8
 */
public interface ExternalRequirement {
    /**
     * Get information about the external requirement management system
     * associated with this requirement.
     * 
     * @return instance of {@link RequirementProviderInfo}, guaranteed to be
     *         included in the values returned by
     *         {@link Project#getRequirementProviders()}
     */
    RequirementProviderInfo getProviderInfo();
    
    /**
     * Get the identifier of this requirement in the external requirement
     * management system.
     * 
     * @return the identifier as string or null if not available
     */
    String getExternalIdentifier();
}
