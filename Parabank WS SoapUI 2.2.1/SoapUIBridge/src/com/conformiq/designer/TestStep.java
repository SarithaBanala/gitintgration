/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import com.conformiq.qtronic2.TimeStamp;

public interface TestStep {

	enum StepType {
		CHECKPOINT,
		EXTERNAL_MESSAGE,
		INTERNAL_MESSAGE,
		EXECUTION_INFO,
		SECTION_INFO,
		
		/**
		 * @since API version 5
		 */
		CREATOR_ACTION
	}
	
	/**
	 * Returns a type of the test step.
	 *
	 * @return a type of the step
	 */
	StepType getType();
	
	/**
	 * Returns a time stamp of the test step in the test case.
	 *
	 * @return a time stamp of the step
	 */
	TimeStamp getTimeStamp();
}
