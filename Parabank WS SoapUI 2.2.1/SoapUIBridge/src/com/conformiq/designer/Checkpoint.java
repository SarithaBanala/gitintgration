/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import java.util.Collection;
import java.util.List;

/**
 * Interface for checkpoints.
 * <p>
 * Some types of checkpoints implement a subinterface with more information
 * about the checkpoint.
 * 
 * @see Requirement
 */
public interface Checkpoint extends CheckpointBase
{
	/**
	 * Type of a checkpoint.
	 */
    enum CheckpointType {
        /**
         * A requirement checkpoint.
         * <p>
         * Implements {@link Requirement}.
         */
		REQUIREMENT,
		
		STATE,
		TRANSITION,
		TRANSITION_PAIR,
		
		BOUNDARY,
		/**
		 * @deprecated as of API version 8
		 */
		@Deprecated
		BRANCH,
		CONDITION,
		
		METHOD,
		STATEMENT,
		
        OTHER,
        
        /**
         * @since API version 5
         */
		AD_ACTION_INVOCATION,
		
		/**
		 * @since API version 5
		 */
		AD_CONTROL_FLOW,
		
		/**
		 * @since API version 5
		 */
		AD_NODE,
		
        /**
         * @since API version 5
         */
		DATA,
		
		/**
		 * @since API version 5
		 */
		DECISION,
		
		/**
		 * @since API version 8
		 */
		COMPOUND,
		
		/**
		 * @since API version 8
		 */
		MCDC,
		
		/**
		 * @since API version 8
		 */
		MC,

		/**
         * @since API version 11
         */
		COMBINATION,

        /**
         * @since API version 11
         */
		IMPLICIT_CONSUMPTION,

        /**
         * @since API version 11
         */
		AD_OBJECT_FLOW,

        /**
         * @since API version 11
         */
		EXCEPTION,

        /**
         * @since API version 11
         */
		PRECONDITION,

        /**
         * @since API version 11
         */
		USER_DEFINED,

		/**
		 * @since API version 11
		 */
		USE_CASE
	}

	/**
	 * Return type of this checkpoint
	 */
	CheckpointType getType();

	/**
	 * Return unique identifier of this checkpoint used by external tools,
	 * or <code>null</code>.
	 * 
	 * Returned unique identifier of the checkpoint is unique within
	 * the model. No other checkpoint has same extension identifier.
	 * If this checkpoint does not link to any external tool's
	 * checkpoint, this method returns <code>null</code>.
	 * 
	 * @deprecated This method no longer works and always returns null. Use
	 *             {@link #getExternalRequirements()} instead.
	 */
	@Deprecated
	String getExtensionIdentifier();

	/**
	 * Returns <code>true</code> if and only if this checkpoint has been covered
	 * by some test case within given design configuration.
	 * 
	 * @param dc	a design configuration whose coverage status will be resolved
	 * @return		whether some test case covers this checkpoint
	 */
	boolean isCovered(DesignConfiguration dc);
	
	/**
	 * Returns <code>true</code> if and only if this checkpoint has been covered
	 * in a test body of some test case within given design configuration.
	 * 
	 * @param dc	a design configuration whose coverage status will be resolved
	 * @return		whether some test case covers this checkpoint
	 */
	boolean isBodyCovered(DesignConfiguration dc);
	
	/**
	 * Returns a list of test cases which cover this checkpoint in given design
	 * configuration.
	 * 
	 * @param dc
	 *            a design configuration whose test case covering will be
	 *            returned
	 * @return a list of test cases covering this checkpoint
	 * 
	 * @deprecated This method can be very slow. It is recommended to redesign
	 *             code such that it is not necessary to use this.
	 */
	@Deprecated
	List<TestCase> getCoveringTests(DesignConfiguration dc);

	/**
	 * Returns a list of test cases which cover this checkpoint in given design configuration in
	 * test body, if any.
	 * 
	 * If a test case does not have section markers, then whole test case is the body, and return
	 * value of this method equals to {@link Checkpoint#getCoveringTests(DesignConfiguration)}.
	 * 
	 * @param dc	a design configuration whose test case covering will be returned
	 * @return		a list of test cases covering this checkpoint
	 * 
	 * @deprecated This method can be very slow. It is recommended to redesign
	 *             code such that it is not necessary to use this.
	 */
	@Deprecated
	List<TestCase> getBodyCoveringTests(DesignConfiguration dc);

	/**
	 * Returns a collection of external requirements associated with this checkpoint.
	 * The collection is always empty if the checkpoint type is not REQUIREMENT.
	 * 
	 * @return external requirements associated with this checkpoint
	 * @since API version 8
	 */
	Collection<ExternalRequirement> getExternalRequirements();
}
