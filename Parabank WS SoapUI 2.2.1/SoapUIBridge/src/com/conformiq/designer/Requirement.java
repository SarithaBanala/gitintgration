/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

/**
 * Interface for requirement checkpoints.
 * 
 * @since API version 11 
 */
public interface Requirement extends Checkpoint {

    /**
     * Returns the requirement identifier associated with this checkpoint.
     * 
     * @return requirement identifier associated with this checkpoint.
     */
    String getRequirementIdentifier();

    /**
     * Returns the requirement description associated with this checkpoint.
     * 
     * @return requirement description associated with this checkpoint, or null
     *         if there's no description.
     */
    String getRequirementDescription();
}
