/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

/**
 * Every data import plugin must implement this interface.
 */
public interface ModelingFrontendPlugin
{
    /**
     * Conformiq Designer Client calls this for those files under "frontend" directory that the plugin is
     * interested in handling, when those files change or when "model import"
     * action is triggered.
     * <p>
     * All problems with input data must be signaled by calling notify from
     * the callback, thereby creating a marker to the input file.
     * Problems not related to input data are signaled by throwing exceptions.
     * These create markers to the plugin node itself on the Project Explorer.
     *
     * @param path  Relative path to the imported file
     *
     * @return True to indicate that the operation was successfully completed,
     *         False otherwise.
     */
    public boolean importModelArtifact(java.lang.String path);

    /**
     * Set callback channel that can be used to
     * <ol>
     * <li>Write out the a file
     * <li>Notify the end user about errors
     * </ol>
     * This routine is called by QEC immediately after the model frontend
     * object has been constructed.
     */
    public void setCallbackSink(ModelingFrontendCallback sink);

    /**
     * Set value of configuration option. Plugin can use this method to
     * get access to two kinds of configuration options:
     * <ol>
     * <li>Configuration options that are set in the Conformiq Designer user
     *     interface (f.ex. used testing heuristics and model level
     *     coverage options).
     * <li>User defined configuration options that are based on the XML
     *     document 'Configuration.xml' inside the JAR file of the plugin.
     * </ol>
     * In the case there is a subtree in the user defined configuration
     * option, property contains tree in dot separated format
     * (e.g. "dir1.dir2.item"). Return value indicates if this is
     * acceptable value for this property or not. E.g. if property is TCP
     * port number and user enters non-number, Plugin should return
     * <code>false</code>.
     * <p>
     * This routine is called by Conformiq Designer Client possibly multiple times right after
     * setting the callback sink.
     */
    public boolean setConfigurationOption(
            java.lang.String property,
            java.lang.String value);
}
