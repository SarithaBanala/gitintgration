/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.designer;

import com.conformiq.qtronic2.NotificationSink;

/**
 * Interface for plug-ins' callback interface for both notification messages 
 * and progress from a plug-in to the end-user.  An object that
 * implements the script back-end interface communicates with an object that
 * implements the ProgressNotificationSink interface.  In the basic setting,
 * Conformiq's testing tool implements ProgressNotificationSink, and receives
 * notifications from plug-ins.
 */
public interface ProgressNotificationSink extends NotificationSink {

	/**
	 * Notify about increase of progress in the task.
	 * 
	 * When cumulative progress reaches 100 units, then task
	 * is assumed to be completed.
	 * 
	 * @param progress	a measure of progress since last time called
	 */
	void progress(int progress);
}
