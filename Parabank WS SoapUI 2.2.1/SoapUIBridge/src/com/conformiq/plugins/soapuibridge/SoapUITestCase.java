/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

import java.util.HashMap;
import java.util.Map;

import com.conformiq.creator.CreatorAction;
import com.conformiq.creator.CreatorActionStep;
import com.conformiq.creator.domain.generic.CreatorGenericAction;
import com.conformiq.creator.domain.generic.CreatorMessageAction;
import com.conformiq.designer.ProgressNotificationSink;
import com.conformiq.designer.TestCase;
import com.conformiq.designer.TestStep;
import com.conformiq.qtronic2.NotificationSink.Severity;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlTestSuite;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;

public class SoapUITestCase {

    private final WsdlTestCase testCase;
    private final ProgressNotificationSink ns;
    private final WsdlInterface[] interfaces;

    public SoapUITestCase(WsdlTestSuite testsuite, TestCase creatorTestCase, WsdlProject project,
            ProgressNotificationSink ns, WsdlInterface[] interfaces) {
        this.interfaces = interfaces;
        testCase = testsuite.addNewTestCase(creatorTestCase.getName());
        this.ns = ns;
        addTestSteps(creatorTestCase);
    }

    private void addTestSteps(TestCase creatorTestCase) {
        XMLGenerator xmlGenerator = new XMLGenerator(ns);
        Map<String, SoapUIPropertyTransferReference> placeholderRefrencesCollected = new HashMap<>();
        CreatorMessageAction request = null;
        for (TestStep testStep : creatorTestCase) {
            switch (testStep.getType()) {
            case CHECKPOINT:
            case EXECUTION_INFO:
            case EXTERNAL_MESSAGE:
            case INTERNAL_MESSAGE:
            case SECTION_INFO:
                break;
            case CREATOR_ACTION:
                CreatorActionStep creatorActionStep = (CreatorActionStep)testStep;
                CreatorAction creatorAction = creatorActionStep.getAction();
                switch (creatorAction.getActionDomain()) {
                case GENERIC:
                    CreatorGenericAction creatorGenericAction = (CreatorGenericAction)creatorAction;
                    switch (creatorGenericAction.getActionKind()) {
                    case CUSTOM:
                        break;
                    case MESSAGE:
                        CreatorMessageAction creatorMessageAction = (CreatorMessageAction)creatorAction;
                        switch (creatorMessageAction.getActionType()) {
                        case INPUT:
                            if (request != null) {
                                errorOrException(creatorTestCase.getName() + ": Ignoring request '"
                                        + request.getMessage().getType().getName() + "' without a response.");
                            }
                            request = creatorMessageAction;
                            break;
                        case VERIFICATION:
                            if (request == null) {
                                errorOrException(creatorTestCase.getName() + ": Ignoring response '"
                                        + creatorMessageAction.getMessage().getType().getName()
                                        + "' without a previous request.");
                            } else {
                                handleSoapTransaction(request, creatorMessageAction, xmlGenerator,
                                        placeholderRefrencesCollected);
                                request = null;
                            }
                        }
                        break;
                    }
                    break;
                case GUI:
                    break;
                }
                break;
            }
        }
    }

    private void errorOrException(String string) {
        ns.notify(Severity.WARNING, string);
    }

    private void handleSoapTransaction(CreatorMessageAction request, CreatorMessageAction response,
            XMLGenerator xmlGenerator, Map<String, SoapUIPropertyTransferReference> placeholderRefrencesCollected) {
        new SoapUISoapRequest(this.testCase, request.getMessage(), request.getInterface().getName(),
                response.getMessage(), xmlGenerator, interfaces, placeholderRefrencesCollected);

    }

}
