/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *     Heikki Tauriainen - check for failures in SoapUIProject.saveAs
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

import java.io.File;
import java.io.IOException;

import org.apache.xmlbeans.XmlException;

import com.conformiq.designer.DesignConfiguration;
import com.conformiq.designer.ProgressNotificationSink;
import com.conformiq.designer.TestCase;
import com.eviware.soapui.impl.WsdlInterfaceFactory;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlProjectFactory;
import com.eviware.soapui.impl.wsdl.WsdlTestSuite;
import com.eviware.soapui.model.project.SaveStatus;
import com.eviware.soapui.support.SoapUIException;

public class SoapUIProject {

    private WsdlProject project;
    private WsdlTestSuite suite;

    public SoapUIProject(DesignConfiguration dc, ProgressNotificationSink ns, String projectName, String wSDLFileOrUrl)
            throws XmlException, IOException, SoapUIException {

        WsdlProjectFactory projectFactory = new WsdlProjectFactory();
        project = projectFactory.createNew();
        project.setName(projectName);
        ns.notify(ProgressNotificationSink.Severity.INFO, "(2/5) Importing WSDL from '" + wSDLFileOrUrl + "'...");        
        WsdlInterface[] interfaces = WsdlInterfaceFactory.importWsdl(project, wSDLFileOrUrl, true);
        ns.notify(ProgressNotificationSink.Severity.INFO, "(3/5) Creating SoapUI test suite...");
        suite = project.addNewTestSuite("Conformiq Generated");
        ns.notify(ProgressNotificationSink.Severity.INFO, "(4/5) Processing test cases...");
        for (TestCase testCase : dc) {
            add(new SoapUITestCase(suite, testCase, project, ns, interfaces));
        }
    }

    private void add(SoapUITestCase soapUiTestCase) {

    }

    public void saveAs(File f) throws IOException {
        try {
            if (project.saveIn(f) != SaveStatus.SUCCESS) {
                throw new IOException("Failed to save project.");
            }
        } finally {
            project.release();
        }
    }

}
