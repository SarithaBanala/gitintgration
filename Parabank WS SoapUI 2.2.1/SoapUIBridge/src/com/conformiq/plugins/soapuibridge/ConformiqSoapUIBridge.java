/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *     Heikki Tauriainen - add input file version check, document command line interface, refactor error reporting for use with scripters 
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.conformiq.designer.DesignConfiguration;
import com.conformiq.designer.ProgressNotificationSink;
import com.conformiq.pluginapi.serializable.Serializer;
import com.conformiq.qtronic2.NotificationSink;
import com.eviware.soapui.SoapUI;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlProjectFactory;
import com.eviware.soapui.impl.wsdl.support.http.SoapUIMultiThreadedHttpConnectionManager;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCaseRunner;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStep;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStepResult;
import com.eviware.soapui.model.testsuite.TestCase;
import com.eviware.soapui.model.testsuite.TestStep;
import com.eviware.soapui.model.testsuite.TestStepResult.TestStepStatus;
import com.eviware.soapui.model.testsuite.TestSuite;
import com.eviware.soapui.support.types.StringToObjectMap;

public class ConformiqSoapUIBridge {

    private static final String SOAPUIBRIDGE_VERSION = "1.0_" + getVersionFromManifest();
    private static final String SOAPUI_LOG_DIRECTORY = "SoapUI_logs";

    private static final String getVersionFromManifest() {
        String result = null;
        try {
            final Class<ConformiqSoapUIBridge> c = ConformiqSoapUIBridge.class;
            final String className = c.getSimpleName() + ".class";
            final String classPath = c.getResource(className).toString();
            if (classPath.startsWith("jar")) {
                final String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1)
                        + "/META-INF/MANIFEST.MF";
                final java.util.jar.Manifest manifest = new java.util.jar.Manifest(
                        new java.net.URL(manifestPath).openStream());
                final java.util.jar.Attributes attr = manifest.getMainAttributes();
                result = attr.getValue("Implementation-Version");
            }
        } catch (final IOException e) {
        }
        return (result != null ? result : "unknown");
    }

    private static class ArgumentError extends RuntimeException {
        private static final long serialVersionUID = -8792211339418220820L;

        ArgumentError(final String msg) {
            super(msg);
        }
    }

    private static class OperationFailed extends RuntimeException {
        private static final long serialVersionUID = -4682804931058994588L;

        OperationFailed(final String msg) {
            super(msg);
        }
    }

    private static final ProgressNotificationSink ns = new ProgressNotificationSink() {

        @Override
        public void notify(Severity notificationClass, String message) {
            ((notificationClass == Severity.INFO || notificationClass == Severity.DEBUG) ? System.out : System.err)
                    .println(notificationClass + ":" + message);
        }

        @Override
        public void progress(int progress) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void notify(String notificationClass, String message) {
            throw new UnsupportedOperationException();

        }
    };

    private static void initSoapUIOperation(final String operation, final int expected_num_parameters,
            final int num_parameters) {
        ns.notify(NotificationSink.Severity.INFO,
                "SoapUIBridge " + SOAPUIBRIDGE_VERSION + " - Conformiq test suite converter for SoapUI");
        ns.notify(NotificationSink.Severity.INFO, "Copyright (c) 2016 Conformiq, Inc. and its subsidiaries.");
        if (expected_num_parameters != num_parameters) {
            throw new ArgumentError(operation + " mode requires " + expected_num_parameters + " additional parameter"
                    + (expected_num_parameters != 1 ? "s" : "") + ", but "
                    + (num_parameters == 0 ? "none" : (num_parameters < 4 ? "only " : "") + num_parameters) + " "
                    + (num_parameters == 1 ? "was" : "were") + " specified");
        }
        // Set directory for SoapUI log files
        final File logDir = new File(SOAPUI_LOG_DIRECTORY);
        if (logDir.isDirectory() || logDir.mkdir()) {
            System.setProperty("soapui.logroot", SOAPUI_LOG_DIRECTORY + "/");
        } else {
            ns.notify(NotificationSink.Severity.WARNING,
                    "Failed to create directory for SoapUI logs, using current working directory");
        }
    }

    private static void generate(final String[] args) {
        initSoapUIOperation("--generate", 4, args.length - 1);

        final String inputFileName = args[1];
        final String outputDirectoryName = args[2];
        final String projectName = args[3];
        final String wsdlLocation = args[4];

        ns.notify(NotificationSink.Severity.INFO, "(1/5) Reading test suite from '" + inputFileName + "'...");

        FileInputStream fis = null;
        final DesignConfiguration dc;
        try {
            fis = new FileInputStream(new File(inputFileName));
            dc = Serializer.Read(fis);
        } catch (final IOException | ClassNotFoundException e) {
            throw new OperationFailed("Failed to read test suite from '" + inputFileName + "': " + e.getMessage());
        } catch (final Serializer.HeaderMismatch e) {
            throw new OperationFailed(
                    "File '" + inputFileName + "' does not appear to be a SerializerScripter output file");
        } catch (final Serializer.SerializationAPIVersionMismatch e) {
            throw new OperationFailed("File '" + inputFileName + "' has been generated by an incompatible version ("
                    + e.getOffendingVersionNumber()
                    + ") of SerializerScripter and cannot be processed using this version of SoapUIBridge (expected version: "
                    + Serializer.getSerializationAPIVersion() + ")");
        } catch (final Serializer.PluginAPIVersionMismatch e) {
            throw new OperationFailed("The scripter plug-in API version (" + e.getOffendingVersionNumber() + ") of '"
                    + inputFileName + "' is not supported by this version of SoapUIBridge (expected version number: "
                    + Serializer.getScripterPluginAPIVersion() + ")");
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (final IOException e) {
                }
            }
        }

        final SoapUIProject pr;
        try {
            pr = new SoapUIProject(dc, ns, projectName, wsdlLocation);
        } catch (final Exception e) {
            throw new OperationFailed("Failed to create SoapUI project: " + e.getMessage());
        }

        ns.notify(NotificationSink.Severity.INFO, "(5/5) Saving generated SoapUI project...");

        final File outFile = new File(outputDirectoryName, projectName + ".xml");
        try {
            pr.saveAs(outFile);
        } catch (final Exception e) {
            throw new OperationFailed(
                    "Failed to save SoapUI project into '" + outFile.getAbsolutePath() + ": " + e.getMessage());
        } finally {
            shutdown();
        }
        ns.notify(NotificationSink.Severity.INFO, "Successfully created SoapUI project <hyperlink=\""
                + outFile.getAbsolutePath() + "\">" + outFile.getAbsolutePath() + "</hyperlink>.");
    }

    private static void execute(final String[] args) {
        initSoapUIOperation("--execute", 4, args.length - 1);

        final String projectFileName = args[1];
        final String testSuiteName = args[2];
        final String testCaseDesignator = args[3];
        final String testStepDesignator = args[4];

        try {
            final WsdlProjectFactory projectFactory = new WsdlProjectFactory();
            final WsdlProject project;
            try {
                project = projectFactory.createNew();
                project.loadProject(new FileInputStream(projectFileName));
            } catch (final Exception e) {
                throw new OperationFailed(
                        "Failed to load SoapUI project from '" + projectFileName + "': " + e.getMessage());
            }

            final TestSuite testsuite = project.getTestSuiteByName(testSuiteName);
            if (testsuite == null) {
                throw new OperationFailed("Test suite '" + testSuiteName + "' not found in '" + projectFileName + "'.");
            }
            TestCase testCase = null;
            try {
                testCase = testsuite.getTestCaseAt(Integer.parseInt(testCaseDesignator));
            } catch (final IndexOutOfBoundsException e) {
                throw new OperationFailed("No test case at index " + testCaseDesignator + " in test suite '"
                        + testSuiteName + "' in '" + projectFileName + "'.");
            } catch (final NumberFormatException e) {
                testCase = testsuite.getTestCaseByName(testCaseDesignator);
                if (testCase == null) {
                    throw new OperationFailed("Test case '" + testCaseDesignator + "' not found in test suite '"
                            + testSuiteName + "' in '" + projectFileName + "'.");
                }
            }

            final StringToObjectMap map = new StringToObjectMap();
            final WsdlTestCaseRunner runner = new WsdlTestCaseRunner((WsdlTestCase)testCase, map);
            TestStep testStep = null;
            try {
                testStep = testCase.getTestStepAt(Integer.parseInt(testStepDesignator));
            } catch (final IndexOutOfBoundsException e) {
                throw new OperationFailed("No test step at index " + testStepDesignator + " in test case '"
                        + testCase.getName() + "' in test suite '" + testSuiteName + "' in '" + projectFileName + "'.");
            } catch (final NumberFormatException e) {
                testStep = testCase.getTestStepByName(testStepDesignator);
                if (testStep == null) {
                    throw new OperationFailed(
                            "Test step '" + testStepDesignator + "' not found in test case '" + testCase.getName()
                                    + "' in test suite '" + testSuiteName + "' in '" + projectFileName + "'.");
                }
            }

            final WsdlTestRequestStep wsdlTestStep = (WsdlTestRequestStep)testStep;
            final WsdlTestRequestStepResult result = (WsdlTestRequestStepResult)(wsdlTestStep.run(runner,
                    runner.createContext(map)));
            if (result.getStatus() == TestStepStatus.OK) {
                ns.notify(NotificationSink.Severity.INFO, "Test executed successfully.");
            } else {
                ns.notify(NotificationSink.Severity.ERROR, "Failed to execute test step!");
                ns.notify(NotificationSink.Severity.ERROR, "Actual response content:");
                ns.notify(NotificationSink.Severity.ERROR, result.getResponseContent());
                for (final String message : result.getMessages()) {
                    ns.notify(NotificationSink.Severity.ERROR, message);
                }
                throw new OperationFailed("Test execution failed.");
            }
        } finally {
            shutdown();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException, InterruptedException {
        int exitstatus = 0;
        try {
            boolean fail_on_arguments = true;
            if (args.length > 0) {
                if (args[0].equals("-g") || args[0].equals("--generate")) {
                    generate(args);
                    fail_on_arguments = false;
                } else if (args[0].equals("-e") || args[0].equals("--execute")) {
                    execute(args);
                    fail_on_arguments = false;
                } else if (args[0].equals("-h") || args[0].equals("--help")) {
                    System.out.println(
                            "SoapUIBridge " + SOAPUIBRIDGE_VERSION + " - Conformiq test suite converter for SoapUI\n\n"
                                    + "Copyright (c) 2016 Conformiq Inc. and its subsidiaries.  SoapUIBridge is made\n"
                                    + "available under the terms of the Eclipse Public License v1.0 which accompanies\n"
                                    + "this distribution, and is available at\n"
                                    + "http://www.eclipse.org/legal/epl-v10.html.\n\n"
                                    + "SoapUIBridge depends on, and includes an unmodified copy of SoapUI Open Source\n"
                                    + "v5.2.1 (http://www.soapui.org/), which is copyright (C) 2004-2014\n"
                                    + "smartbear.com, and distributed under the European Union Public License (EUPL)\n"
                                    + "V. 1.1.  The SoapUI license, and the licenses of its supporting components,\n"
                                    + "are available in the 3rdparty-licenses subdirectory in the SoapUIBridge\n"
                                    + "distribution.\n\n" + "Usage:\n"
                                    + "    java -jar SoapUIBridge.jar --generate [INFILE] [OUTDIR] [PROJECT] [WSDL]\n"
                                    + "    java -jar SoapUIBridge.jar --execute [PROJECT] [SUITEID] [CASEID] [STEPID]\n"
                                    + "    java -jar SoapUIBridge.jar --help\n\n"
                                    + "With --generate (alternatively, -g), SoapUIBridge will create a SoapUI project\n"
                                    + "from a test suite exported from Conformiq Creator using the SerializerScripter\n"
                                    + "scripting back-end.  Here,\n"
                                    + "    [INFILE]  is the name of a SerializerScripter scripting back-end output\n"
                                    + "              file,\n"
                                    + "    [OUTDIR]  is the name of the directory in which to save output files,\n"
                                    + "    [PROJECT] is a name for the SoapUI project (the output file will be saved\n"
                                    + "              to [PROJECT].xml in [OUTDIR]), and\n"
                                    + "    [WSDL]    is the URL or location of a WSDL file with message definitions.\n\n"
                                    + "In the --execute (-e) mode, SoapUIBridge will execute a test step from a test\n"
                                    + "case in a SoapUI project.  Here,\n"
                                    + "    [PROJECT] is the name of a SoapUI project file,\n"
                                    + "    [SUITEID] is the name of a test suite in the SoapUI project\n"
                                    + "    [CASEID]  is the numeric identifier, or the name of a test case in the\n"
                                    + "              test suite, and\n"
                                    + "    [STEPID]  is the numeric identifier, or the name of a test step in the\n"
                                    + "              test case.\n");
                    fail_on_arguments = false;
                }
            }
            if (fail_on_arguments) {
                throw new ArgumentError(
                        "please specify --generate, --execute or --help as the first command line argument");
            }
        } catch (final OperationFailed e) {
            ns.notify(NotificationSink.Severity.ERROR, e.getMessage());
            exitstatus = 1;
        } catch (final ArgumentError e) {
            ns.notify(NotificationSink.Severity.ERROR, "Cannot start SoapUIBridge: " + e.getMessage());
            exitstatus = 2;
        } catch (final Exception e) {
            ns.notify(NotificationSink.Severity.ERROR, "Error running SoapUIBridge: " + e.getMessage());
            e.printStackTrace();
            exitstatus = 3;
        }
        System.exit(exitstatus);
    }

    public static final void shutdown() {
        // Need to shut down all the threads invoked by each SoapUI TestSuite
        SoapUI.getThreadPool().shutdown();
        try {
            SoapUI.getThreadPool().awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new OperationFailed("Failed: SoapUI.getThreadPool().awaitTermination(1, TimeUnit.SECONDS);");
        }

        // Now to shut down the monitor thread setup by SoapUI
        Thread[] tarray = new Thread[Thread.activeCount()];
        Thread.enumerate(tarray);
        for (Thread t : tarray) {
            if (t instanceof SoapUIMultiThreadedHttpConnectionManager.IdleConnectionMonitorThread) {
                ((SoapUIMultiThreadedHttpConnectionManager.IdleConnectionMonitorThread)t).shutdown();
            }
        }
        SoapUI.shutdown();
    }

}
