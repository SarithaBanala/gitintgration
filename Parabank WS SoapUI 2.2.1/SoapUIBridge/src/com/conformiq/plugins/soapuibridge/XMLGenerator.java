/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

import java.io.StringWriter;
import java.util.FormatFlagsConversionMismatchException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.generic.CreatorExternalInterface;
import com.conformiq.creator.domain.generic.CreatorGenericType;
import com.conformiq.creator.domain.generic.CreatorGenericType.Type;
import com.conformiq.creator.domain.generic.value.CreatorBooleanValue;
import com.conformiq.creator.domain.generic.value.CreatorChoiceValue;
import com.conformiq.creator.domain.generic.value.CreatorEnumValue;
import com.conformiq.creator.domain.generic.value.CreatorListValue;
import com.conformiq.creator.domain.generic.value.CreatorNumberValue;
import com.conformiq.creator.domain.generic.value.CreatorOmittedValue;
import com.conformiq.creator.domain.generic.value.CreatorSequenceValue;
import com.conformiq.creator.domain.generic.value.CreatorStringValue;
import com.conformiq.creator.domain.generic.value.CreatorStructuredValue;
import com.conformiq.creator.domain.generic.value.CreatorValue;
import com.conformiq.creator.domain.generic.value.CreatorValueField;
import com.conformiq.designer.ProgressNotificationSink;
import com.conformiq.qtronic2.NotificationSink.Severity;

public class XMLGenerator {
    private final ProgressNotificationSink mNotificationSink;

    public XMLGenerator(ProgressNotificationSink mNotificationSink) {
        this.mNotificationSink = mNotificationSink;
    }

    private Map<String, String> mNamespacePrefixes = new HashMap<String, String>();

    private String mCurrentTestcaseName;

    public static String getAddress(CreatorExternalInterface port) {
        return port.getAnnotations().get("soap:address");

    }

    private boolean isPresent(CreatorValue value) {
        return !(value instanceof CreatorOmittedValue);
    }

    private void registerNameSpace(String namespace) {
        if (!mNamespacePrefixes.containsKey(namespace)) {
            mNamespacePrefixes.put(namespace, "ns" + mNamespacePrefixes.size()); //$NON-NLS-1$
        }
    }

    /**
     * 
     * @param root
     *            will collect namespace attributes
     * @param current
     */
    private void setNameSpacePrefixes(Node root, Node current) {
        debug("Registered name spaces: " + mNamespacePrefixes); //$NON-NLS-1$
        String ns = current.getNamespaceURI();
        debugUnique("Trying to set ns prefix: for node: " + current.getNodeName() + ":" + ns); //$NON-NLS-1$ //$NON-NLS-2$
        if (ns != null) {
            String prefix = mNamespacePrefixes.get(ns);
            if (prefix != null) {
                NamedNodeMap attributes = root.getAttributes();
                Attr attr = root.getOwnerDocument().createAttribute("xmlns:" + prefix); //$NON-NLS-1$
                attr.setValue(ns);
                attributes.setNamedItem(attr);
                debugUnique("Setting ns: " + prefix + " for node: " + current.getNodeName()); //$NON-NLS-1$ //$NON-NLS-2$
                current.setPrefix(prefix);
            }
        }
        NodeList children = current.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node item = children.item(i);
            setNameSpacePrefixes(root, item);
        }
    }

    public static class SoapRequestMessageAsXmlAndCollectMacros {
        public String mSoapRequestMessageAsXml;

        private void setSoapRequestMessageAsXml(String soapRequestMessageAsXml) {
            this.mSoapRequestMessageAsXml = soapRequestMessageAsXml;
        }

        public String getSoapRequestMessageAsXml() {
            return this.mSoapRequestMessageAsXml;
        }

    }

    public SoapRequestMessageAsXmlAndCollectMacros buildSoapRequestMessageAsXmlAndCollectMacros(
            CreatorStructuredValue message, String interfaceName,
            List<SoapUIPropertyTransferReference> propertyTransferTargets, String stepName) {
        SoapRequestMessageAsXmlAndCollectMacros ret = new SoapRequestMessageAsXmlAndCollectMacros();
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element soapEnvelope = doc.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", //$NON-NLS-1$
                    "SOAP-ENV:Envelope"); //$NON-NLS-1$
            doc.appendChild(soapEnvelope);

            List<CreatorValueField> fields = message.getFields();
            for (CreatorValueField field : fields) {
                CreatorValueWrapper<CreatorValue> valueWrapper = field.getValue();
                if (valueWrapper.isDontCare()) {
                    continue;
                }
                CreatorValue fieldValue = field.getValue().get();
                if (isPresent(fieldValue)) // $NON-NLS-1$
                {
                    String xpath = "//*[local-name()='Envelope']";// appendElementToXpath("//*[local-name()='Envelope']", //$NON-NLS-1$
                                                                  // field.getName());

                    if (fieldValue != null && isPresent(fieldValue) && fieldValue.getType() != null) {
                        Type fieldValueType = fieldValue.getType().getType();
                        switch (fieldValueType) {
                        case SEQUENCE:
                            buildSoapRequestAsXml(soapEnvelope, field.getValue(), field.getName(),
                                    field.getType().getNamespace(), xpath, propertyTransferTargets, stepName);
                            break;
                        case LIST:
                        case BOOLEAN:
                        case CHOICE:
                        case CUSTOMACTION:
                        case DATE:
                        case ENUMERATION:
                        case MESSAGE:
                        case NUMBER:
                        case STRING:
                            throw new IllegalStateException();
                        }
                    }
                }
            }

            List<Node> schemaValidationWork = new LinkedList<Node>();

            {
                NodeList soapBodies = doc.getElementsByTagNameNS("http://schemas.xmlsoap.org/soap/envelope/", "Body");
                if (soapBodies.getLength() > 1) {
                    throw new IllegalArgumentException(mCurrentTestcaseName + " - More than one SOAP Body detected");
                } else if (soapBodies.getLength() == 1) {
                    Node body = soapBodies.item(0);
                    NodeList bodyChildren = body.getChildNodes();
                    for (int i = 0; i < bodyChildren.getLength(); i++) {
                        Node bodyChild = bodyChildren.item(i);
                        schemaValidationWork.add(bodyChild);
                        setNameSpacePrefixes(bodyChild, bodyChild);
                    }
                }
            }

            {
                NodeList soapHeaders = doc.getElementsByTagNameNS("http://schemas.xmlsoap.org/soap/envelope/",
                        "Header");
                if (soapHeaders.getLength() > 1) {
                    throw new IllegalArgumentException("More than one SOAP Header detected");
                } else if (soapHeaders.getLength() == 1) {
                    Node header = soapHeaders.item(0);
                    NodeList headerChildren = header.getChildNodes();
                    for (int i = 0; i < headerChildren.getLength(); i++) {
                        Node headerChild = headerChildren.item(i);
                        setNameSpacePrefixes(headerChild, headerChild);
                    }
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2"); //$NON-NLS-1$ //$NON-NLS-2$
            StringWriter xmlOutput = new StringWriter();
            StreamResult sr = new StreamResult(xmlOutput);
            transformer.transform(new DOMSource(doc), sr);
            ret.setSoapRequestMessageAsXml(xmlOutput.toString());
            return ret;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String appendElementToXpath(String xpath, String element) {
        // return xpath + "." + element; //$NON-NLS-1$
        return String.format("%s/*[local-name()='%s']", xpath, element);
    }

    private Element appendChild(Element elelment, String fieldName, String namespaceName) {
        if (namespaceName != null && namespaceName.endsWith("wsdl")) //$NON-NLS-1$
        {
            errorUnique("Field: '%s' has probably wrong namespace name: '%s'", fieldName, namespaceName); //$NON-NLS-1$
        }
        Element el = null;
        if (namespaceName == null) {
            throw new NullPointerException("Namespace is null");
        } else if (namespaceName.isEmpty()) {
            el = elelment.getOwnerDocument().createElementNS(null, fieldName);
        } else {
            el = elelment.getOwnerDocument().createElementNS(namespaceName, fieldName);
        }

        debugUnique("Creating element: " + fieldName + " ns=" + namespaceName); //$NON-NLS-1$ //$NON-NLS-2$
        registerNameSpace(namespaceName);
        elelment.appendChild(el);
        return el;
    }

    private void buildSoapRequestAsXml(Element elelment, CreatorValueWrapper<CreatorValue> valueWrapper,
            String fieldName, String namespaceName, String xpath,
            List<SoapUIPropertyTransferReference> propertyTransferTargets, String stepName) {
        if (valueWrapper.isDontCare()) {
            return;
        }

        CreatorValue value = valueWrapper.get();
        xpath = appendElementToXpath(xpath, fieldName);

        if (isPresent(value)) {

            CreatorGenericType type = value.getType();
            switch (type.getType()) {
            case ENUMERATION: {
                CreatorEnumValue enValue = (CreatorEnumValue)value;
                Element el = appendChild(elelment, fieldName, namespaceName);
                el.setTextContent(enValue.getValue());
                // if(mUseDatabase)
                // {
                // mMysqlConnector.resolveMaysqlVariableReference(xpath, enValue.getValue());
                // }
                break;
            }
            case SEQUENCE: {
                CreatorSequenceValue seqValue = (CreatorSequenceValue)value;
                List<CreatorValueField> fields = seqValue.getFields();
                Element el = appendChild(elelment, fieldName, namespaceName);
                for (CreatorValueField nextField : fields) {
                    if (nextField.getValue() != null) {
                        buildSoapRequestAsXml(el, nextField.getValue(), nextField.getName(),
                                nextField.getType().getNamespace(), xpath, propertyTransferTargets, stepName);
                    }
                }
                break;
            }
            case STRING: {
                CreatorStringValue strVAlue = (CreatorStringValue)value;
                String string = strVAlue.getString();
                Element el = appendChild(elelment, fieldName, namespaceName);
                el.setTextContent(string);
                if (string.startsWith("${") && string.endsWith("}")) {
                    String placeholder = string.substring("${".length(), string.length() - "}".length());
                    propertyTransferTargets.add(new SoapUIPropertyTransferReference(stepName, xpath, placeholder));

                }
                // if(mUseDatabase)
                // {
                // mMysqlConnector.resolveMaysqlVariableReference(xpath, strVAlue.getString());
                // }
                break;
            }
            case NUMBER: {
                CreatorNumberValue numValue = (CreatorNumberValue)value;
                Element el = appendChild(elelment, fieldName, namespaceName);
                el.setTextContent(numValue.getNumber().toString());
                // if(mUseDatabase)
                // {
                // mMysqlConnector.resolveMaysqlVariableReference(xpath, numValue.getNumber().toString());
                // }
                break;
            }
            case LIST: {
                CreatorListValue arrayValue = (CreatorListValue)value;
                List<CreatorValueWrapper<CreatorValue>> fields = arrayValue.getContents();
                for (CreatorValueWrapper<CreatorValue> nextField : fields) {
                    // Element el = appendChild(elelment, fieldName, namespaceName);
                    buildSoapRequestAsXml(elelment, nextField, fieldName, namespaceName, xpath, propertyTransferTargets,
                            stepName);
                }
                break;
            }
            case BOOLEAN: {
                CreatorBooleanValue booleanValue = (CreatorBooleanValue)value;
                Element el = appendChild(elelment, fieldName, namespaceName);
                el.setTextContent(Boolean.toString(booleanValue.getBoolean()));
                // if(mUseDatabase)
                // {
                // mMysqlConnector.resolveMaysqlVariableReference(xpath, Boolean.toString(booleanValue.getBoolean()));
                // }
                break;
            }
            case CHOICE: {
                CreatorChoiceValue seqValue = (CreatorChoiceValue)value;
                List<CreatorValueField> fields = seqValue.getFields();
                Element el = appendChild(elelment, fieldName, namespaceName);
                for (CreatorValueField nextField : fields) {
                    if (nextField.getValue() != null) {
                        buildSoapRequestAsXml(el, nextField.getValue(), nextField.getName(),
                                nextField.getType().getNamespace(), xpath, propertyTransferTargets, stepName);
                    }
                }
                break;
            }

            case CUSTOMACTION:
            case DATE:
            case MESSAGE:
                notSupported(type.getType());
            }
        }
    }

    private void notSupported(Type type) {
        throw new UnsupportedOperationException();

    }

    Set<String> mReferencedDbRows;
    Set<String> mReferencedDbColumns;

    Set<String> mReferencedVariables;

    private void debug(String msg) {
        {
            mNotificationSink.notify(Severity.DEBUG, msg); // $NON-NLS-1$
        }
    }

    private void debugUnique(String msg) {
        if (debugs.add(msg)) {
            debug(msg);
        }
    }

    private Set<String> debugs = new HashSet<String>();

    private void error(String msg, Object... params) {
        try {
            mNotificationSink.notify(Severity.ERROR, String.format(msg, params)); // $NON-NLS-1$
        } catch (FormatFlagsConversionMismatchException e) {
            throw new RuntimeException("msg: " + msg + "params: " + params, e);
        }
    }

    private void errorUnique(String msg, Object... params) {
        msg = String.format(msg, params);
        if (errors.add(msg)) {
            error(msg);
        }
    }

    private Set<String> errors = new HashSet<String>();

}
