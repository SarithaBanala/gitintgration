/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import com.conformiq.creator.domain.generic.value.CreatorMessageValue;
import com.conformiq.creator.domain.generic.value.CreatorValue;
import com.conformiq.pluginapi.serializable.SerializableCreatorValueWrapperImpl;
import com.conformiq.plugins.soapuibridge.XMLGenerator.SoapRequestMessageAsXmlAndCollectMacros;
import com.eviware.soapui.config.TestStepConfig;
import com.eviware.soapui.config.WsdlRequestConfig;
import com.eviware.soapui.impl.wsdl.AbstractWsdlModelItem;
import com.eviware.soapui.impl.wsdl.WsdlContentPart;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlOperation;
import com.eviware.soapui.impl.wsdl.WsdlRequest;
import com.eviware.soapui.impl.wsdl.testcase.WsdlTestCase;
import com.eviware.soapui.impl.wsdl.teststeps.PathLanguage;
import com.eviware.soapui.impl.wsdl.teststeps.PropertyTransfer;
import com.eviware.soapui.impl.wsdl.teststeps.PropertyTransfersTestStep;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStep;
import com.eviware.soapui.impl.wsdl.teststeps.registry.PropertyTransfersStepFactory;
import com.eviware.soapui.impl.wsdl.teststeps.registry.WsdlTestRequestStepFactory;
import com.eviware.soapui.model.iface.MessagePart;

public class SoapUISoapRequest {

    private SoapRequestMessageAsXmlAndCollectMacros xmlRequestAsString;

    public SoapUISoapRequest(WsdlTestCase testCase, CreatorMessageValue requestMessage, String interfaceName,
            CreatorMessageValue responseMessage, XMLGenerator xmlGenerator, WsdlInterface[] interfaces,
            Map<String, SoapUIPropertyTransferReference> propertyTransferSources) {
        WsdlOperation operation = findOperation(requestMessage, interfaces);
        if (operation == null) {
            throw new IllegalStateException("Operation not found");
        }
        List<SoapUIPropertyTransferReference> propertyTransferTargets = new LinkedList<>();

        String stepName = operation.getName();
        xmlRequestAsString = xmlGenerator.buildSoapRequestMessageAsXmlAndCollectMacros(requestMessage, interfaceName,
                propertyTransferTargets, stepName);

        for (SoapUIPropertyTransferReference propertyTransferTarget : propertyTransferTargets) {
            SoapUIPropertyTransferReference propertyTranserSource = propertyTransferSources
                    .get(propertyTransferTarget.placeholder);

            PropertyTransfersStepFactory propertyTransfersStepFactory = new PropertyTransfersStepFactory();

            TestStepConfig propertyTransferStepConfig = propertyTransfersStepFactory.createNewTestStep(testCase,
                    propertyTranserSource.stepName + ":" + propertyTranserSource.placeholder);
            PropertyTransfersTestStep propertyTransferStep = (PropertyTransfersTestStep)testCase
                    .addTestStep(propertyTransferStepConfig);

            PropertyTransfer propertyTransferSettings = propertyTransferStep
                    .addTransfer(propertyTranserSource.stepName + ":" + propertyTranserSource.placeholder);
            propertyTransferSettings.setSourcePath(propertyTranserSource.xpath);
            propertyTransferSettings.setSourcePathLanguage(PathLanguage.XPATH);
            propertyTransferSettings.setSourceStepName(propertyTranserSource.stepName);
            propertyTransferSettings.setSourcePropertyName("Response");

            propertyTransferSettings.setTargetPath(propertyTransferTarget.xpath);
            propertyTransferSettings.setTargetPathLanguage(PathLanguage.XPATH);
            propertyTransferSettings.setTargetStepName(propertyTransferTarget.stepName);
            propertyTransferSettings.setTargetPropertyName("Request");

        }

        WsdlRequestConfig callConfig = WsdlRequestConfig.Factory.newInstance();
        WsdlRequest request = new WsdlRequest(operation, callConfig);

        request.setRequestContent(xmlRequestAsString.getSoapRequestMessageAsXml());
        TestStepConfig c = WsdlTestRequestStepFactory.createConfig(request, stepName);
        WsdlTestRequestStep step = (WsdlTestRequestStep)testCase.addTestStep(c);
        SoapUIAssertion.buildAssertions(step, new SerializableCreatorValueWrapperImpl(responseMessage), false, "Envelope", "/",
                new LinkedList<SoapUIAssertion>(), propertyTransferSources);
    }

    private WsdlOperation findOperation(CreatorValue message, WsdlInterface[] interfaces) {
        WsdlOperation found = null;
        for (WsdlInterface interphace : interfaces) {
            List<AbstractWsdlModelItem<?>> messages = interphace.getAllMessages();
            for (AbstractWsdlModelItem<?> msg : messages) {
                WsdlRequest r = (WsdlRequest)msg;
                MessagePart[] qqq = r.getRequestParts();
                for (MessagePart qq : qqq) {
                    if (qq instanceof WsdlContentPart) {
                        WsdlContentPart q = (WsdlContentPart)qq;
                        QName abc = q.getPartElementName();
                        if (("SOAP Envelope for " + abc.getLocalPart()).equals(message.getType().getName())) {
                            found = r.getOperation();
                        }

                    }
                }
            }
        }
        return found;
    }
}
