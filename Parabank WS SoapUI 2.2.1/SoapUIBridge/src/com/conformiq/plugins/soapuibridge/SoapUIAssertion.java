/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.conformiq.creator.CreatorValueWrapper;
import com.conformiq.creator.domain.generic.value.CreatorBooleanValue;
import com.conformiq.creator.domain.generic.value.CreatorEnumValue;
import com.conformiq.creator.domain.generic.value.CreatorListValue;
import com.conformiq.creator.domain.generic.value.CreatorNumberValue;
import com.conformiq.creator.domain.generic.value.CreatorOmittedValue;
import com.conformiq.creator.domain.generic.value.CreatorStringValue;
import com.conformiq.creator.domain.generic.value.CreatorStructuredValue;
import com.conformiq.creator.domain.generic.value.CreatorValue;
import com.conformiq.creator.domain.generic.value.CreatorValueField;
import com.eviware.soapui.impl.wsdl.teststeps.WsdlTestRequestStep;
import com.eviware.soapui.impl.wsdl.teststeps.assertions.basic.XPathContainsAssertion;

public class SoapUIAssertion {

    public SoapUIAssertion(XPathContainsAssertion assertion) {
    }

    private static boolean isPresent(CreatorValue value) {
        return !(value instanceof CreatorOmittedValue);
    }

    Set<String> mReferencedDbRows;
    Set<String> mReferencedDbColumns;

    public static void buildAssertions(WsdlTestRequestStep step, CreatorValueWrapper<CreatorValue> valueWrapper,
            boolean isOptional, String fieldName, String xpath, List<SoapUIAssertion> mAssertionsCollected,
            Map<String, SoapUIPropertyTransferReference> propertyTransferSources) {
        if (valueWrapper.isDontCare()) {
            return;
        }
        CreatorValue value = valueWrapper.get();
        if (value != null) {
            String newXpath = String.format("%s/*[local-name()='%s']", xpath, fieldName);

            if (isOptional) {

                if (!isPresent(value)) {
                    // XXX Handle optional value is present assertion
                }
            }
            if (isPresent(value)) {
                switch (value.getType().getType()) {
                case LIST:
                    CreatorListValue arrayValue = (CreatorListValue)value;
                    List<CreatorValueWrapper<CreatorValue>> arrayContent = arrayValue.getContents();
                    if (arrayContent.size() > 1) {
                        // XXX Handle multiple occurance assertion
                    } else {
                        // XXX Handle single occurance assertion
                    }
                    break;
                case BOOLEAN:
                    CreatorBooleanValue booleanValue = (CreatorBooleanValue)value;
                    mAssertionsCollected.add(buildValueAssertion(step, fieldName,
                            Boolean.toString(booleanValue.getBoolean()), newXpath));
                    break;
                case CHOICE:
                case SEQUENCE:
                case MESSAGE:
                    CreatorStructuredValue creatorStructuredValue = (CreatorStructuredValue)value;
                    for (CreatorValueField field : creatorStructuredValue.getFields()) {
                        buildAssertions(step, field.getValue(), field.getType().isOptional(), field.getName(), newXpath,
                                mAssertionsCollected, propertyTransferSources);
                    }
                    break;
                case CUSTOMACTION:
                    throw new UnsupportedOperationException();
                case DATE:
                    throw new UnsupportedOperationException();
                case ENUMERATION:
                    CreatorEnumValue enumValue = (CreatorEnumValue)value;
                    mAssertionsCollected.add(buildValueAssertion(step, fieldName, enumValue.getValue(), newXpath));
                    break;
                case NUMBER:
                    CreatorNumberValue numberValue = (CreatorNumberValue)value;
                    mAssertionsCollected
                            .add(buildValueAssertion(step, fieldName, numberValue.getNumber().toString(), newXpath));
                    break;
                case STRING:
                    CreatorStringValue creatorStringValue = (CreatorStringValue)value;
                    String stringValue = creatorStringValue.getString();
                    if (stringValue.startsWith("${") && stringValue.endsWith("}")) {
                        String placeholder = stringValue.substring("${".length(), stringValue.length() - "}".length());
                        propertyTransferSources.put(placeholder,
                                new SoapUIPropertyTransferReference(step.getName(), newXpath, placeholder));
                    } else {
                        mAssertionsCollected.add(buildValueAssertion(step, fieldName, stringValue, newXpath));
                    }
                    break;
                }
            }
        }
    }

    private static SoapUIAssertion buildValueAssertion(WsdlTestRequestStep step, String fieldName, String value,
            String newXpath) {
        XPathContainsAssertion assertion = (XPathContainsAssertion)step.addAssertion("XPath Match");
        assertion.setName(fieldName);
        assertion.setPath(newXpath);
        assertion.setExpectedContent(value);
        return new SoapUIAssertion(assertion);
    }
}
