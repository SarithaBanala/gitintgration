/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. SoapUIBridge is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Evgeny Gornov - initial implementation
 *******************************************************************************/
package com.conformiq.plugins.soapuibridge;

public class SoapUIPropertyTransferReference {

    public final String stepName;
    public final String xpath;
    public final String placeholder;

    public SoapUIPropertyTransferReference(String stepName, String xpath, String placeholder) {
        this.stepName = stepName;
        this.xpath = xpath;
        this.placeholder = placeholder;
    }

}
