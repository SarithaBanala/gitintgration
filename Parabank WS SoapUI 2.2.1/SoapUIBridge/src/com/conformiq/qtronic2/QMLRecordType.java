/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import java.util.Collection;

/**
 * Interface for QMLRecordType.
 */
public interface QMLRecordType extends QMLType
{
    /**
     * Number of record fields.
     */
    public int getNumberOfFields();

    /**
     * Get record fields (type, field name pairs).
     */
    public QMLRecordTypeField getField(int idx);

    /**
     * Get record fields.
     * 
     * @since API version 4
     */
    public Collection<QMLRecordTypeField> getFields();
    
    /**
     * @since API version 5
     */
    public QMLRecordTypeField getFieldByIdentifier(String identifier);
    
    /**
     * Number of inner record types.
     */
    public int getNumberOfInnerRecords();

    /**
     * Get inner type by index. Index starts from zero (0),
     * maximum being one less than number of inner record types as
     * returned by {@code int getNumberOfInnerRecords()}. 
     */
    public QMLRecordType getInnerType(int idx);

    /**
     * Get inner types.
     * 
     * @since API version 4
     */
    public Collection<QMLRecordType> getInnerTypes();
}
