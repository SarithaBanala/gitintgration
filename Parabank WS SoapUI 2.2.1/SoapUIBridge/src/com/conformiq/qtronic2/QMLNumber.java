/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import java.math.BigInteger;

/**
 * Interface for QMLNumber.
 */
public interface QMLNumber extends QMLValue
{
    /**
     * Is the number an integer?
     * 
     * @return true if the type of the number value is any integral type,
     *         otherwise false
     */
    public boolean isInteger();

    /**
     * Is the number a double?
     * 
     * @return true if the type of the number value is any non-integral type,
     *         otherwise false
     */
    public boolean isDouble();

    /**
     * Get the integer value.
     */
    public long getInteger();

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void setInteger(long value);

    /**
     * Get the double value.
     */
    public double getDouble();

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void setDouble(double value);

    /**
     * Is the number available as big integer?
     */
    public boolean isBigInteger();
    
    /**
     * Get the value as big integer.
     */
    public BigInteger getBigInteger();

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void setBigInteger(BigInteger value);

    /**
     * Get the value as big rational. Exact value is always available as
     * arbitrary length rational number.
     */
    public BigInteger getNumerator();
    public BigInteger getDenominator();

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void setRational(BigInteger nominator, BigInteger denominator);
}
