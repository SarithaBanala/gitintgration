/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Interface for checkpoints
 */
public interface Checkpoint
{
    /**
     * Type of checkpoint.
     */
    public class CheckpointType
    {
        /**
         * Normal checkpoint
         */
        public static final int USUAL_CHECKPOINT = 0;

        /**
         * Requirement
         */
        public static final int REQUIREMENT = 1;
    }

    /**
     * Checkpoint log item types.
     */
    public class CheckpointStatus
    {
        /**
         * @deprecated Checkpoint statically unreachable. This
         * status code is deprecated, and not used by Conformiq
         * Designer.
         */
        @Deprecated
        public static final int UNREACHABLE = 0;

        /**
         * @deprecated Checkpoint unreachable for the rest of the
         * run. This status code is deprecated, and not used by
         * Conformiq Designer.
         */
        @Deprecated
        public static final int UNREACHABLE_HERE = 1;

        /**
         * Checkpoint not covered in this run yet.
         */
        public static final int UNCOVERED = 2;

        /**
         * @deprecated Checkpoint may have been covered during this
         * run. This status code is deprecated, and not used by
         * Conformiq Designer.
         */
        @Deprecated
        public static final int MAYBE_COVERED = 3;

        /**
         * Checkpoint has been covered during this run.
         */
        public static final int COVERED = 4;
    }

    /**
     * Return name of this checkpoint.
     * 
     * The format of the string returned by this method has been changed in API version 11.
     * 
     * @see com.conformiq.designer.Checkpoint#getShortName()
     */
    java.lang.String getName();

    /**
     * Return type of this checkpoint
     */
    int getType();

    /**
     * Return the full name of this checkpoint
     * 
     * The full name of a checkpoint contains slash '/' separated components of the checkpoint name as one long string.
     *
     * @since API version 11
     */
    String getFullName();

    /**
     * Return unique internal name of this checkpoint.
     * 
     * Returned unique internal name of the checkpoint is unique within the model. 
     * No other checkpoint has same identifier.
     * 
     * This is guaranteed to be unique, however it is not safe to parse this String as there is no guarantee about its
     * format.
     * 
     * @since API version 11
     */
    String getIdentifier();

    /**
     * Return type of this checkpoint
     * 
     * @since API version 11
     */
    com.conformiq.designer.Checkpoint.CheckpointType getCheckpointType();

    /**
     * For requirement checkpoints, return the requirement description or null if there's no description.
     * For other checkpoints, throw UnsupportedOperationException.
     * 
     * @since API version 11
     */
    String getRequirementDescription();
}
