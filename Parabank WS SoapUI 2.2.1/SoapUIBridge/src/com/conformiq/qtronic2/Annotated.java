/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import java.util.Map;

/** 
 * @since API version 5
 */
public interface Annotated {

	/**
	 * Annotations related to the type.
	 */
	public Map<String, Annotation> getAnnotations();

	/**
	 * Get value of a specific annotation or null if no such annotation exists
	 * for the type.
	 */
	public Annotation getAnnotation(String id);
}