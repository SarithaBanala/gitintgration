/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import java.util.Collection;

/**
 * Interface for QMLRecord.
 */
public interface QMLRecord extends QMLValue
{
    /**
     * Get name of this record's type.
     */
    public java.lang.String getName();

    /**
     * Get the number of fields in this record.
     */
    public int getNumberOfFields();

    /**
     * Get a field at the given index. It is an error to index out of
     * bounds.
     */
    public QMLRecordField getField(int idx);
    
    /**
     * @since API version 4
     */
    public Collection<QMLRecordField> getFields();

    /**
     * Get value of the given field by field name.
     */
    public QMLRecordField getField(java.lang.String field);
    
    /**
     * Get value of given field by identifier.
     * 
     * @since API version 5
     */
    public QMLRecordField getFieldByIdentifier(String field);

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void setField(java.lang.String field, QMLValue value);
}
