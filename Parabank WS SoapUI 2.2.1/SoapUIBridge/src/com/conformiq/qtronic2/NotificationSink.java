/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Interface for plugins' callback interface.  An object that
 * implements the {@link Plugin} interface communicates with an object that
 * implements the NotificationSink interface.  In the basic setting,
 * Conformiq's testing tool implements NotificationSink, and receives
 * notifications from plugins.
 */
public interface NotificationSink
{
    /**
     * Send extra information about Plugin's state.
     *
     * @deprecated since API version 11, use {@link #notify(Severity severity, java.lang.String message)} instead
     * @param notificationClass  describes the type of the notification; valid values are "info",
     *                           "warning" and "error"
     * @param message             the actual information piece
     */
    @Deprecated
    public void notify(java.lang.String notificationClass,
                       java.lang.String message);


    /**
     * Notification severity levels.
     * @since API version 11
     */
    enum Severity {
        /**
         * Severity level for things that are usually not interesting to a
         * regular user but that are useful when investigating issues.
         */
        DEBUG("debug"),
        /**
         * Severity level for informational notifications. These are relevant to
         * a regular user and state things that are part of normal progress.
         */
        INFO("info"),
        /**
         * Severity level for non-fatal issues that do not prevent successful completion.
         */
        WARNING("warning"),
        /**
         * Severity level for fatal errors. These mean that outcome is not
         * successful and outputs may be corrupted.
         */
        ERROR("error");

        Severity(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }

        String name;
    }

    /**
     * Send notification to show on client.
     * @since API version 11
     * @param severity is this normal progress or some issue
     * @param message information content
     */
    public void notify(Severity severity, java.lang.String message);
}
