/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Scripting and Logging backends work in synchronous manner, and this is
 * an super interface for both of those.
 */
abstract class SynchronousPlugin extends Plugin
{
    /**
     * QML language has a predefined trace() function for displaying messages
     * in the log window of Conformiq Designer while testing. Conformiq
     * Designer invokes Trace() function of each connected script and logger
     * plugin once this expression is executed by the Conformiq Designer
     * engine.
     * 
     * @param message   the outputted trace message
     * @param time      the timestamp at which the trace() function is executed by Conformiq
     *                  Designer
     */
    public abstract boolean trace(java.lang.String message, TimeStamp time);

    /**
     * Render an internal message take-over, ie. an internal
     * communication step. Return true to indicate success and false
     * to indicate an abnormal condition (test script rendering will
     * be aborted).
     * <p>
     * The purpose of a call of this method is to render a single
     * internal communications step, i.e. a single message take-over
     * between internal threads in the model.
     * 
     * @param datum      the datum that is sent
     * @param sender     the thread sending the datum
     * @param receiver   the thread that received the datum
     * @param port       the internal port through which the datum is
     *                   transported
     * @param time       the time when the message is sent and received
     */
    public abstract boolean internalCommunicationsInfo(
        QMLRecord datum,
        java.lang.String sender, java.lang.String receiver,
        java.lang.String port, TimeStamp time);
}
