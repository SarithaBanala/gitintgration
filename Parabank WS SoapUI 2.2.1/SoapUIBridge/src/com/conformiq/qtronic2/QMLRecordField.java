/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * A field of a QML record.
 */
public interface QMLRecordField
{
    /**
     * Get field definition of this field.
     */
    public QMLRecordTypeField getFieldType();

    /**
     * Record field has a field name.
     */
    public java.lang.String getName();
    
    /**
     * QML identifier of the field.
     * 
     * @since API version 5
     */
    public String getIdentifier();

    /**
     * Record field has a value.
     */
    public QMLValue getValue();

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void setValue(QMLValue value);
}
