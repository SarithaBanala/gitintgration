/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Interface for QMLRecordTypeFields.
 */
public interface QMLRecordTypeField extends Annotated
{
    /**
     * Get type of the field.
     */
    public QMLType getType();

    /**
     * Get the full type name of the field.
     * For example "A.B" for an inner record type of B
     * in a record type A.
     */
    public java.lang.String getTypeName();

    /**
     * Get field name.
     */
    public java.lang.String getFieldName();
    
    /**
     * Get field QML identifier.
     * 
     * @since API version 5
     */
    public String getFieldIdentifier();
}
