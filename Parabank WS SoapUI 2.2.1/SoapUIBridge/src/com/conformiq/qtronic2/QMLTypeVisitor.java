/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Visitor interface for visiting QMLTypes.
 */
public interface QMLTypeVisitor
{
    /**
     * Visit a QML array.
     */
    public void visit(QMLArrayType a);

    /**
     * Visit a QML boolean.
     */
    public void visit(QMLBooleanType b);

    /**
     * Visit a QML number.
     */
    public void visit(QMLNumberType n);

    /**
     * Visit a QML record.
     */
    public void visit(QMLRecordType r);

    /**
     * Visit a QML string.
     */
    public void visit(QMLStringType s);

    /**
     * Visit a QML optional type.
     */
    public void visit(QMLOptionalType p);

    /**
     * Visit a QML enum type.
     */
    public void visit(QMLEnumType p);
}
