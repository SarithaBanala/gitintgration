/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * This interface is used by scripters to configure script host options.
 *
 * @since API version 3
 */
public interface HostConfigurator
{
	/**
	 * Enables or disables partial test case / test preconditions.
	 * 
	 * @param enable  whether to enable partial test cases / preconditions
	 */
	void setPreconditionsEnabled(boolean enable);
}
