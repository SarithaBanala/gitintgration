/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import com.conformiq.designer.PortDefinition;

/**
 * This is a dictionary for metadata of a model.
 * 
 * @deprecated Use {@link com.conformiq.designer.QMLMetadata} instead.
 */
@Deprecated
public interface MetaDataDictionary
{
    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public QMLValue get(java.lang.String key);

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public java.lang.String getNextKey(java.lang.String key);
    
    /**
     * Get all defined ports.
     * 
     * @since API version 4
     */
    public PortDefinition[] getPorts();
    
    /**
     * Find a specific port.
     * 
     * @since API version 4
     */
    public PortDefinition findPort(String name);

    /**
     * Get all the top level QML record types defined in the model.
     * The result does not contain inner record types.
     */
    public java.util.Vector<QMLRecordType> getTypes();

    /**
     * Access type by its name.
     */
    public QMLRecordType getType(java.lang.String name);

    /**
     * Access primitive types.
     */
    public QMLBooleanType getBooleanType();
    public QMLStringType getStringType();
    public QMLNumberType getByteType();
    public QMLNumberType getCharType();
    public QMLNumberType getShortType();
    public QMLNumberType getIntType();
    public QMLNumberType getLongType();
    public QMLNumberType getFloatType();
    public QMLNumberType getDoubleType();
}
