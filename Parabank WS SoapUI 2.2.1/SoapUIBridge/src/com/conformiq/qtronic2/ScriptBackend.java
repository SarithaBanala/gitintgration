/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import com.conformiq.designer.PortDefinition;

/**
 * Abstract base class for script back-ends. Script back-ends render test
 * scripts when Conformiq Designer works in the offline test script generation
 * mode.
 * 
 * @deprecated Use {@link com.conformiq.designer.QueryScriptBackend} instead.
 */
@Deprecated
public abstract class ScriptBackend extends SynchronousPlugin
{
    /**
     * Begin script is the first method called.
     *
     * @param dcName   the name of the test design configuration from which the
     *                 test suite ws produced
     * @return         <code>true</code> to indicate success. If the method
     *                 returns <code>false</code>, then the script rendering
     *                 will not continue
     */
    public abstract boolean beginScript(java.lang.String dcName);

    /**
     * Set scripter host options. This method is called after {@link #beginScript(String)}.
     *  
     * @param host  configuration interface for setting script host options
     * 
     * @since API version 3
     */
    public void setHostOptions(HostConfigurator host) { }

    /**
     * Begin test case. This method is called zero or more times
     * (usually more) after a call to {@link #beginScript(String)}.
     *
     * @param testcaseName  the name of this testcase
     * @return              <code>true</code> to indicate success and
     *                      <code>false</code> to indicate an abnormal condition (test
     *                      script rendering will be aborted)
     */
    public abstract boolean beginCase(java.lang.String testcaseName);

    /**
     * Begin test case. As above, except that {@code stream} is a string stream
     * where the test case is to be written to. By default, throw
     * UnsupportedOperationException to indicate that the given scripter does
     * not support writing test cases to streams.
     */
    public boolean beginCase(java.lang.String testcaseName,
                             java.lang.StringBuilder stream)
    { throw new java.lang.UnsupportedOperationException(); }

    /**
     * Set test case probability. This method should be called after
     * {@link #beginCase(String)} if test case probability is set.
     */
    public abstract void caseProbability(double probability);

    /**
     * Test case description. The test case description is generated from the
     * "narrative" fragments that are used to build a narrative of what
     * happens in a test case from the system perspective.
     */
    public void caseDescription(java.lang.String description) { }

    /**
     * Set numeric "Test Case ID" defined by Conformiq Designer. This numeric
     * ID can be used to maintain mapping from the given test case to the one
     * in the Conformiq Designer user interface (in cases where you cannot
     * derive the ID directly from the test case name).
     */
    public void caseID(int id) { }

    /**
     * Render a test step. This method is called zero or more times (usually
     * more) after a call to beginCase(). Return true to indicate success and
     * false to indicate an abnormal condition (test script rendering will be
     * aborted).
     * <p>
     * The purpose of a call of this method is to render a single test step,
     * i.e.  a single test message either in the inbound or the outbound
     * direction.  The first argument {@code datum} is the datum that is either the
     * sent or the expected message. The string {@code thread} is the name of
     * thread in the model that is expected to send or receive the
     * message. The string {@code port} is the inbound our outbound port through
     * which the datum must be or should be transported. The {@code isFromTester}
     * flag indicates the direction. This information is derived, because all
     * the ports are unidirectional on this level. Finally, {@code timestamp} is
     * the required or expected time when the message must be sent or
     * received. */
    public abstract boolean testStep(QMLRecord datum,
                                     java.lang.String thread,
                                     java.lang.String port,
                                     boolean isFromTester, TimeStamp ts);
    
    /**
     * @since API version 4
     */
    public boolean testStep(QMLRecord datum,
                            String thread,
                            PortDefinition port,
                            boolean isFromTester,
                            TimeStamp ts) {
    	return testStep(datum, thread, port.getName(), isFromTester, ts);
    }
    
    /**
     * @since API version 4
     */
    public boolean internalCommunicationsInfo(
            QMLRecord datum,
            String sender,
            String receiver,
            PortDefinition port,
            TimeStamp time) {
    	return internalCommunicationsInfo(datum, sender, receiver, port.getName(), time);
    }


    /**
     * Render checkpoint information. This method is called zero or more times
     * after a call to {@link #beginCase(String)}. Return <code>true</code> to indicate success and false
     * to indicate an abnormal condition (test script rendering will be
     * aborted).
     * <p>
     * The purpose of a call of this method is to render information about
     * model-driven test coverage in the generated test script. This should
     * not affect test execution at all, so it is possible to generate valid
     * and executable test scripts while ignoring all calls to this
     * method. However, if you are interested in how the test scripts are
     * mapped to model-driven coverage you can benefit from implementing this
     * method properly.
     *
     * @param checkpoint  the checkpoint whose status is reported at the
     *                    particular point in the test script.
     * @param status      the status
     * @param timestamp   the timestamp at which the checkpoint status becomes known
     *                    (usually the same as the previous message in the test script)
     */
    public abstract boolean checkpointInfo(Checkpoint checkpoint,
                                           int status,
                                           TimeStamp timestamp);

    /**
     * Render checkpoint information. This method is called zero or more times
     * after a call to {@link #beginCase(String)}. Return <code>true</code> to indicate success and false
     * to indicate an abnormal condition (test script rendering will be
     * aborted).
     * <p>
     * The purpose of a call of this method is to render information about
     * model-driven test coverage in the generated test script. This should
     * not affect test execution at all, so it is possible to generate valid
     * and executable test scripts while ignoring all calls to this
     * method. However, if you are interested in how the test scripts are
     * mapped to model-driven coverage you can benefit from implementing this
     * method properly.
     *
     * @param checkpoint  the checkpoint whose status is reported at the
     *                    particular point in the test script.
     * @param status      the status
     * @param timestamp   the timestamp at which the checkpoint status becomes known
     *                    (usually the same as the previous message in the test script)
     * @param thread      gives the name of the thread that covers the given checkpoint
     */
    public boolean checkpointInfo(Checkpoint checkpoint,
                                  int status,
                                  TimeStamp timestamp,
                                  java.lang.String thread)
    {
        return checkpointInfo(checkpoint, status, timestamp);
    }

    /**
     * Section marker. Used by the ISO 9696-1 option to designate the
     * three different sections of a test case.  This
     * method is called whenever a section begins; the end of a section is
     * implicit at the beginning of the next one, or at the end of the
     * test case.
     *
     * @param section_name  argument can be either "Preamble", "Body", or "Postamble"
     */
    public boolean sectionInfo(java.lang.String section_name) { return true; }

    /**
     * Test case scenario. As opposed to "test case narratives", the
     * "scenario" fragments are not considered to be sentences and they do not
     * necessarily form a sequential narrative but are more considered
     * independent labels that together define the present scenario.
     */
    public boolean scenarioInfo(java.lang.String name) { return true; }

    /**
     * Test case narrative. Narrative tags are used to produce a narrative of
     * what happens in a test case from the system perspective.
     */
    public boolean narrativeInfo(java.lang.String name) { return true; }

    /** End test case. Called after {@link #beginCase(String)}.
     *
     * @return <code>true</code> to indicate success, <code>false</code> otherwise
     */
    public abstract boolean endCase();

    /**
     * Output dependency information. The test case with automatically
     * generated name {@code prerequisite} is a prerequisite for test case {@code
     * dependent}; in other words, test case {@code dependent} would likely fail
     * during test execution if test case {@code prerequisite} fails.  Note
     * that it is possible that there are both forward and backward
     * dependencies in the test suite (relative to the order in which test
     * cases are published) even though the goal is to have only backward
     * dependencies (later test cases depend on earlier ones).
     */
    public boolean testCaseDependency(java.lang.String prerequisite,
                                      java.lang.String dependent)
    { return true; }

	/**
	 * Output the name of precondition test case for the given test. The
	 * precondition test needs to be executed before the given test case.
	 * Conformiq Client invokes this routine only if there is a precondition
	 * test and @a precond is always non NULL
	 * 
	 * @since API version 2
	 */
	public boolean casePrecondition(String precond) { return true; }

    /**
     * End test script. Called after {@link #beginScript(String)}.
     *
     * @return <code>true</code> to indicate success, <code>false</code> otherwise
     */
    public abstract boolean endScript();
}
