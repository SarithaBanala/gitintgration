/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import java.util.Collection;

/**
 * Interface for QMLArray.
 */
public interface QMLArray extends QMLValue
{
    /**
     * Get the number of array elements.
     */
    public int getNumberOfElements();

    /**
     * Get the value at the given index. It is an error to index out
     * of array bounds.
     */
    public QMLValue getValue(int idx);
    
    /**
     * Get all element values contained.
     * 
     * @since API version 4
     */
    public Collection<QMLValue> getValues();

    /**
     * Always throws UnsupportedOperationException.
     *
     * @deprecated Since API version 11, always throws UnsupportedOperationException.
     */
	@Deprecated
    public void insertAtEnd(QMLValue value);
}
