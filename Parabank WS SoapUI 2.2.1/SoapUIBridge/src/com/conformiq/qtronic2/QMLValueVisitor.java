/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Visitor interface for visiting QMLValues.
 */
public interface QMLValueVisitor
{
    /**
     * Visit a QML array.
     */
    public void visit(QMLArray a);

    /**
     * Visit a QML boolean.
     */
    public void visit(QMLBoolean b);

    /**
     * Visit a QML number.
     */
    public void visit(QMLNumber n);

    /**
     * Visit a QML record.
     */
    public void visit(QMLRecord r);

    /**
     * Visit a QML string.
     */
    public void visit(QMLString s);

    /**
     * Visit a QML optional type.
     */
    public void visit(QMLOptional p);

    /**
     * Visit a QML enum type.
     */
    public void visit(QMLEnum p);
}
