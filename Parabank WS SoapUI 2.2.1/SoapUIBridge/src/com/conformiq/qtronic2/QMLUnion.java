/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Interface for QMLUnion.
 */
public interface QMLUnion extends QMLRecord
{
    /**
     * Returns the name of the chosen field or <code>null</code> if
     * no field is chosen.
     */
    public java.lang.String chosenField();
    
    /**
     * Returns the identifier of the chosen field or <code>null</code>
     * if no field is chosen.
     * 
     * @since API version 5
     */
    public String chosenFieldIdentifier();

    /**
     * Returns true if the given field is currently chose (active).
     */
    public boolean isChosen(java.lang.String field);
}
