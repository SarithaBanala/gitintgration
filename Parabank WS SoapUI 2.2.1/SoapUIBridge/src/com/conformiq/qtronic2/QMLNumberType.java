/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * Interface for QMLNumberType.
 */
public interface QMLNumberType extends QMLType {
    /**
     * Is the number a byte?
     */
    public boolean isByte();

    /**
     * Is the number a short?
     */
    public boolean isShort();

    /**
     * Is the number an int?
     *
     * Semantics changed in API version 11: Previously also returned true for
     * long, now correctly only returns true for int.
     */
    public boolean isInteger();

    /**
     * Is the number a long?
     *
     * Semantics changed in API version 11: Previously always returned false,
     * now correctly returns true for long.
     */
    public boolean isLong();

    /**
     * Is the number a char?
     *
     * @since API version 10
     */
    public boolean isChar();

    /**
     * Is the number a float?
     *
     * Semantics changed in API version 11: Previously also returned true for
     * double, now correctly only returns true for float.
     */
    public boolean isFloat();

    /**
     * Is the number a double?
     *
     * Semantics changed in API version 11: Previously always returned false,
     * now correctly returns true for double.
     */
    public boolean isDouble();
}
