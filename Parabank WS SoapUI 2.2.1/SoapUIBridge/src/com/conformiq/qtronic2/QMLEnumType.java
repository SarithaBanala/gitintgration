/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

import java.util.List;

/**
 * Interface for QMLEnumType.
 */
public interface QMLEnumType extends QMLType
{
    /**
     * Returns the enum values of this type.
     */
    public List<QMLEnum> getValues();
}
