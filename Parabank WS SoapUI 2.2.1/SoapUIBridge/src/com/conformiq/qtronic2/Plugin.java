/*******************************************************************************
 * Copyright (c) 2016 Conformiq Inc. and its subsidiaries.
 * All rights reserved. This file is part of the Conformiq Plugin API, which is
 * made available under the terms of the Eclipse Public License v1.0 which
 * accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package com.conformiq.qtronic2;

/**
 * This is a high-level base class for all kind of plugins used by Conformiq
 * Designer.
 */
abstract class Plugin
{
    /**
     * Set notification channel that can be used to send out information to
     * the object implementing NotificationSink ie. the Conformiq Designer
     * Eclipse Client. Called after construction to set notification callback
     * object pointer.
     */
    public void setNotificationSink(NotificationSink sink) { }

    /**
     * Set metadata dictionary. Return <code>true</code> if the plugin as capable of
     * receiving the pointer to the metadata dictionary, otherwise <code>false</code>. In
     * practice, always return <code>true</code>. There is a default implementation that
     * returns <code>true</code>.
     */
    public boolean setMetaData(@SuppressWarnings("deprecation") MetaDataDictionary dict) { return true; }

    /**
     * Set value of configuration option. Plugin can use this method to
     * get access to two kinds of configuration options:
     * <ol>
     * <li>Configuration options that are set in the Conformiq Designer user
     *     interface (f.ex. used testing heuristics and model level
     *     coverage options).
     * <li>User defined configuration options that are based on the XML
     *     document 'Configuration.xml' inside the JAR file of the plugin.
     * </ol>
     * <p>
     * In the case there is a subtree in the user defined configuration
     * option, property contains tree in dot separated format
     * (e.g. "dir1.dir2.item"). Return value indicates if this is
     * acceptable value for this property or not. E.g. if property is TCP
     * port number and user enters non-number, Plugin should return
     * <code>false</code>.
     */
    public boolean setConfigurationOption(java.lang.String property,
                                          java.lang.String value) {
        return false;
    }
}
